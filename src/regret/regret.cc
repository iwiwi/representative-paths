#include "regret.h"
#include "geometry/geometry.h"
#include "common/common.h"

namespace regret {
WeightType RegretFromCosts(WeightType current, WeightType optimal) {
  if (IsLessThan(current, optimal)) {
    printf("??? \n%.10f\n%.10f\n", current, optimal);
  }
  assert(IsLessThan(0, current) || IsZero(optimal));
  return IsZero(current) ? 0.0 : (current - optimal) / current;
}

namespace {
WeightType RegretLowerBoundFromPreference(const RegretQuery &query, const WeightVector &preference) {
  WeightType current_cost = kInfinityWeight;
  for (const auto &path : query.paths) {
    current_cost = min(current_cost, path.PersonalizedCost(preference));
  }
  WeightType optimal_cost = query.oracle->Query(preference, nullptr);
  assert(optimal_cost != kInfinityWeight);
  PrintWeightVector(preference);
  return RegretFromCosts(current_cost, optimal_cost);
}
}  // namespace

WeightType RegretRandom(const RegretQuery &query, WeightVector *worst_preference) {
  static const int kNumTrials = 10;  // TODO

  WeightType worst_regret = 0.0;
  for (int i = 0; i < kNumTrials; ++i) {
    WeightVector p = RandomWeightVectorUnitLength(query.oracle->NumDimensions());
    WeightType r = RegretLowerBoundFromPreference(query, p);
    if (r > worst_regret) {
      worst_regret = r;
      if (worst_preference != nullptr) *worst_preference = p;
    }
  }
  return worst_regret;
}

WeightType RegretExact(const RegretQuery &query, WeightVector *worst_preference) {
  vector<WeightVector> points;
  for (const auto &path : query.paths) {
    assert(path.Cost().size() == query.oracle->NumDimensions());
    points.emplace_back(path.Cost());
  }

  vector<geometry::Plane> planes(geometry::LowerEnvelope(points));

  WeightType worst_regret = -1.0;
  for (const auto &plane : planes) {
    WeightVector preference = plane.normal_vector;
    PrintWeightVector(preference); puts("");

    double regret = RegretLowerBoundFromPreference(query, preference);
    if (regret > worst_regret) {
      worst_regret = regret;
      if (worst_preference != nullptr) *worst_preference = preference;
    }
  }
  return worst_regret;
}
}  // namespace regret
