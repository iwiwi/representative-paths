#pragma once
#include "common/common.h"
#include "multi_cost_graph/multi_cost_graph.h"
#include "shortest_path_oracle/shortest_path_oracle.h"

namespace regret {
//
// Compute the regret for a given set of paths
//
struct RegretQuery {
  shared_ptr<shortest_path_oracle::OracleInterface> oracle;
  const vector<multi_cost_graph::Path> &paths;

  RegretQuery(
      shared_ptr<shortest_path_oracle::OracleInterface> oracle,
      const vector<multi_cost_graph::Path> &paths)
    : oracle(oracle), paths(paths) {}
};

WeightType RegretFromCosts(WeightType current, WeightType optimal);

WeightType RegretRandom(const RegretQuery &query, WeightVector *worst_preference);
WeightType RegretExact(const RegretQuery &query, WeightVector *worst_preference);
}  // namespace regret
