#include "representative_paths/representative_paths.h"
#include "multi_cost_graph/multi_cost_graph.h"
#include "common/common.h"
#include <gflags/gflags.h>
#include "utility/eps_image.h"
#include "utility/path_visualizer.h"
using namespace multi_cost_graph;
using namespace utility;

using Point = EPSImage::Point;

DEFINE_string(graph_file, "data/USA-road.NY.mcg", "");
DEFINE_string(coordinate_file, "data/USA-road.NY.co", "");
DEFINE_string(output_file, "out", "");

DEFINE_string(algorithm, "regret_minimizing", "");

DEFINE_double(eps_width, 1024, "");
DEFINE_double(eps_height, 768, "");

DEFINE_int32(from, 0, "");
DEFINE_int32(to, 1, "");
DEFINE_int32(num_paths, 5, "");

int main(int argc, char **argv) {
  google::ParseCommandLineFlags(&argc, &argv, true);

  Graph graph;
  graph.Read(FLAGS_graph_file.c_str());
  utility::GraphCoordinate coordinate;
  coordinate.Read(FLAGS_coordinate_file.c_str());
  CHECK(graph.NumVertices() == coordinate.NumVertices());

  unique_ptr<representative_paths::RepresentativePathsInterface>
  algorithm(representative_paths::RepresentativePathsFromName(FLAGS_algorithm, graph));

  vector<multi_cost_graph::Path> paths;
  JLOG_OPEN("algorithm") {
    JLOG_PUT("name", FLAGS_algorithm);
    JLOG_PUT_BENCHMARK("time_precompute") algorithm->Precompute();
    JLOG_PUT_BENCHMARK("time_query") paths = algorithm->Query(FLAGS_from, FLAGS_to, FLAGS_num_paths);
    JLOG_PUT("num_paths", paths.size());
  }

  PathVisualizer pv(graph, coordinate);
  pv.OutputAll(paths, FLAGS_output_file);

  return 0;
}

