#include <map>
#include <tuple>
#include <sstream>
#include <fstream>
#include <iostream>
#include <gflags/gflags.h>
#include "common/common.h"
#include "multi_cost_graph/multi_cost_graph.h"
#include "utility/graph_coordinate.h"
using namespace std;
using namespace multi_cost_graph;

DEFINE_string(input_time, "", "");
DEFINE_string(input_coordinate, "", "");
DEFINE_string(input_height, "", "");
DEFINE_bool(also_add_energy, false, "");
DEFINE_bool(also_add_unit_cost, false, "");
DEFINE_string(output, "", "");

namespace {
void ReadHeader(istream &ifs, VertexType *num_vs, SizeType *num_es) {
  string line;
  CHECK(getline(ifs, line));
  istringstream ss(line);

  string head;
  CHECK(ss >> head);
  if (head == "c") {
    ReadHeader(ifs, num_vs, num_es);
    return;
  }

  CHECK(head == "p");
  CHECK((ss >> head) && head == "sp");
  CHECK(ss >> *num_vs >> *num_es);
}

bool ReadEdge(istream &ifs, VertexType *s, VertexType *t, WeightType *w) {
  string line;
  if (!getline(ifs, line) || line == "") return false;
  istringstream ss(line);

  string head;
  CHECK(ss >> head);
  if (head == "c") return ReadEdge(ifs, s, t, w);

  CHECK(head == "a");
  return (bool)(ss >> *s >> *t >> *w);
}

vector<tuple<VertexType, VertexType, WeightVector>> ReadEdges() {
  ifstream ifs1(FLAGS_input_time);
  CHECK(ifs1);

  VertexType num_vs1;
  SizeType num_es1;
  ReadHeader(ifs1, &num_vs1, &num_es1);

  vector<tuple<VertexType, VertexType, WeightVector>> edge_tuples;

  SizeType num_es3;
  for (num_es3 = 0; !ifs1.eof(); ++num_es3) {
    VertexType s1, t1;
    WeightType w1;
    bool r1 = ReadEdge(ifs1, &s1, &t1, &w1);
    if (!r1) break;

    CHECK(1 <= s1 && s1 <= num_vs1);
    --s1;
    --t1;

    edge_tuples.emplace_back(s1, t1, WeightVector(1, w1));
  }
  CHECK(ifs1.eof());
  CHECK(num_es1 == num_es3);

  return edge_tuples;
}

vector<WeightType> ReadHeight() {
  ifstream ifs(FLAGS_input_height);
  CHECK(ifs);
  vector<WeightType> heights;
  for (WeightType h; ifs >> h; ) heights.emplace_back(h);
  return heights;
}

template<typename T>
void Write(ostream &is, const T &t) {
  is.write((char*)&t, sizeof(T));
}
}  // namespace

int main(int argc, char **argv) {
  google::ParseCommandLineFlags(&argc, &argv, true);
  std::ios::sync_with_stdio(false);

  vector<tuple<VertexType, VertexType, WeightVector>> edge_tuples(ReadEdges());

  { // Euclid distance
    utility::GraphCoordinate c;
    c.Read(FLAGS_input_coordinate.c_str());
    for (auto &edge : edge_tuples) {
      VertexType u = get<0>(edge);
      VertexType v = get<1>(edge);
      CHECK(u < c.NumVertices() && v < c.NumVertices());
      get<2>(edge).emplace_back(c.Distance(u, v));
    }
  }

  { // Height & Energy
    vector<WeightType> heights;
    if (FLAGS_input_height != "") {
      heights = ReadHeight();
      for (auto &edge : edge_tuples) {
        VertexType u = get<0>(edge);
        VertexType v = get<1>(edge);
        CHECK(u < (VertexType)heights.size() && v < (VertexType)heights.size());
        WeightType h =
            (heights[u] == -32768 || heights[v] == -32768) ? 0 :
                Abs(heights[u] - heights[v]);
        get<2>(edge).emplace_back(h);
      }
    }

    if (FLAGS_also_add_energy) {
      CHECK(FLAGS_input_height != "");
      for (auto &edge : edge_tuples) {
        VertexType u = get<0>(edge);
        VertexType v = get<1>(edge);
        CHECK(u < (VertexType)heights.size() && v < (VertexType)heights.size());
        get<2>(edge).emplace_back(9.832 * max(0.0, heights[u] - heights[v]));
      }
    }
  }

  if (FLAGS_also_add_unit_cost) {
    for (auto &edge : edge_tuples) {
      get<2>(edge).emplace_back(1.0);
    }
  }

  Graph graph;
  graph.Construct(edge_tuples);
  graph.PrintSummary();
  graph.Write(FLAGS_output.c_str());

  return 0;
}
