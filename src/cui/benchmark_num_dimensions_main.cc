#include "multi_cost_graph/multi_cost_graph.h"
#include "multi_cost_graph/dijkstra.h"
#include "regret/regret.h"
#include "shortest_path_oracle/shortest_path_oracle.h"
#include "shortest_path_oracle/flyweight_factory.h"
#include "representative_paths/representative_paths.h"
#include "utility/graph_coordinate.h"
#include "utility/functions.h"
#include <gflags/gflags.h>
using namespace multi_cost_graph;

DEFINE_string(graph_file, "data/USA-road.NY.mcg4", "");
DEFINE_string(coordinate_file, "data/USA-road.NY.co", "");
DEFINE_string(num_dimensions, "2,3,4,5,6,7,8,9,10", "");
DEFINE_string(num_paths, "5,10,15", "");

DEFINE_string(distance_lbs, "1000", "");
DEFINE_string(distance_ubs, "10000", "");
DEFINE_int32(num_pairs, 100, "");
DEFINE_string(memo, "", "");
DEFINE_bool(skip_regret_evaluation, false, "");

int main(int argc, char **argv) {
  JLOG_INIT(&argc, argv);
  google::ParseCommandLineFlags(&argc, &argv, true);

  // Corodinate
  utility::GraphCoordinate coordinate;
  coordinate.Read(FLAGS_coordinate_file.c_str());

  // Command line flags
  const vector<double> distance_lbs =
      utility::ParseCommaSeparatedString<double>(FLAGS_distance_lbs);
  const vector<double> distance_ubs =
      utility::ParseCommaSeparatedString<double>(FLAGS_distance_ubs);
  CHECK(distance_lbs.size() == distance_ubs.size());
  const vector<int> num_paths =
      utility::ParseCommaSeparatedString<int>(FLAGS_num_paths);

  JLOG_OPEN("setting") {
    JLOG_PUT("graph_file", FLAGS_graph_file);
    JLOG_PUT("memo", FLAGS_memo);
    JLOG_PUT("num_paths", FLAGS_num_paths);
    JLOG_PUT("num_dimensions", FLAGS_num_dimensions);
  }

  const vector<SizeType> num_dimensions =
      utility::ParseCommaSeparatedString<SizeType>(FLAGS_num_dimensions);

  // Tested pairs
  vector<vector<pair<VertexType, VertexType>>> tested_pairs(distance_lbs.size());

  for (const SizeType d : num_dimensions) {
    JLOG_ADD_OPEN("num_dimensions_case") {
      JLOG_PUT("num_dimensions", d);

      // Graph
      multi_cost_graph::Graph graph;
      graph.ReadWithSpecifiedDimension(FLAGS_graph_file.c_str(), d);
      CHECK(graph.NumVertices() == coordinate.NumVertices());
      assert(graph.NumDimensions() == d);

      // Shortest path oracle (for evaluation)
      shortest_path_oracle::FlyweightFactory<shortest_path_oracle::BiAStarLazyOracle>::Clear();
      shared_ptr<shortest_path_oracle::OracleInterface>
      oracle = shortest_path_oracle::ShortestPathOracleFromName("bi_astar", graph);

      // The algorithm (to be evaluated)
      unique_ptr<representative_paths::RepresentativePathsInterface>
      algorithm(representative_paths::RepresentativePaths(graph));
      JLOG_PUT("representative_paths_name", algorithm->RepresentativePathsName());
      JLOG_PUT("algorithm_name", algorithm->AlgorithmName());
      JLOG_PUT_BENCHMARK("global.time_precompute") algorithm->Precompute();

      // Evaluation
      for (SizeType j = 0; j < distance_lbs.size(); ++j) {
        JLOG_ADD_OPEN("distance_range") {
          const double distance_lb = distance_lbs[j];
          const double distance_ub = distance_ubs[j];
          JLOG_PUT("distance_lb", distance_lb);
          JLOG_PUT("distance_ub", distance_ub);

          vector<pair<VertexType, VertexType>> &tp = tested_pairs[j];
          if (tp.empty()) {
            tp.resize(FLAGS_num_pairs);
            coordinate.ResetRandom();
            for (int i = 0; i < FLAGS_num_pairs; ++i) {
              tp[i] = coordinate.RandomReachableVertexPairWithinDistanceRange(distance_lb, distance_ub, oracle);
            }
          }

          for (int k : num_paths) {
            JLOG_ADD_OPEN("num_paths_case") {
              JLOG_PUT("num_paths", k);

              for (int p = 0; p < FLAGS_num_pairs; ++p) {
                const auto &pi = tp[p];
                JLOG_ADD_OPEN("pair") {
                  JLOG_PUT("v_from", pi.first);
                  JLOG_PUT("v_to", pi.second);
                  vector<Path> paths;
                  algorithm->ResetStatistics();
                  JLOG_PUT_BENCHMARK("time") {
                    paths = algorithm->Query(pi.first, pi.second, k);
                  }
                  algorithm->OutputStatistics();
                  if (FLAGS_skip_regret_evaluation) continue;
                  JLOG_PUT_BENCHMARK("time_regret_evaluation") {
                    oracle->ResetQueryPoints(pi.first, pi.second);
                    regret::RegretQuery q(oracle, paths);
                    JLOG_PUT("regret", regret::RegretExact(q, nullptr));
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  exit(EXIT_SUCCESS);
}
