#include "multi_cost_graph/multi_cost_graph.h"
#include "representative_paths/representative_paths.h"
#include "shortest_path_oracle/shortest_path_oracle.h"
#include "regret/regret.h"
#include "common/common.h"
#include <gflags/gflags.h>

DEFINE_string(graph_file, "data/USA-road.NY.mcg4", "");
DEFINE_int32(from, 0, "");
DEFINE_int32(to, 1, "");
DEFINE_int32(num_paths, 5, "");
DEFINE_int32(num_dimensions, 0, "");

int main(int argc, char **argv) {
  google::ParseCommandLineFlags(&argc, &argv, true);

  multi_cost_graph::Graph graph;
  if (FLAGS_num_dimensions == 0) {
    graph.Read(FLAGS_graph_file.c_str());
  } else {
    graph.ReadWithSpecifiedDimension(FLAGS_graph_file.c_str(), FLAGS_num_dimensions);
  }

  unique_ptr<representative_paths::RepresentativePathsInterface>
  algorithm(representative_paths::RepresentativePaths(graph));
  JLOG_PUT("algorithm", algorithm->RepresentativePathsName());

  vector<multi_cost_graph::Path> paths;
  BENCHMARK("time") {
    paths = algorithm->Query(FLAGS_from, FLAGS_to, FLAGS_num_paths);
  }
  paths[0].Print();
  PrintWeightVector(paths[0].Cost());

  shared_ptr<shortest_path_oracle::OracleInterface>
  oracle(shortest_path_oracle::ShortestPathOracle(graph));
  oracle->ResetQueryPoints(FLAGS_from, FLAGS_to);
  for (SizeType k = 1; k <= paths.size(); ++k) {
    vector<multi_cost_graph::Path> ps(paths.begin(), paths.begin() + k);
    regret::RegretQuery regret_query(oracle, ps);
    cerr << k << "\t";
    PrintWeightVector(paths[k - 1].Cost());
    cerr << "\t" << regret::RegretExact(regret_query, nullptr)
      << "\t" << regret::RegretRandom(regret_query, nullptr) << endl;
  }

  exit(EXIT_SUCCESS);
}
