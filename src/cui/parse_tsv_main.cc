#include "common/common.h"
#include "multi_cost_graph/multi_cost_graph.h"
using namespace multi_cost_graph;

DEFINE_string(input, "", "");
DEFINE_string(output, "", "");

int main(int argc, char **argv) {
  google::ParseCommandLineFlags(&argc, &argv, true);
  std::ios::sync_with_stdio(false);

  ifstream ifs(FLAGS_input);
  CHECK(ifs);

  vector<tuple<VertexType, VertexType, WeightVector>> edge_tuples;

  for (string line; getline(ifs, line); ) {
    if (line.empty()) continue;

    istringstream ss(line);

    VertexType from, to;
    CHECK(ss >> from >> to);

    WeightVector cost;
    for (WeightType c; ss >> c; ) cost.emplace_back(c);

    edge_tuples.emplace_back(from, to, cost);
  }

  Graph graph;
  graph.Construct(edge_tuples);
  graph.PrintSummary();
  graph.Write(FLAGS_output.c_str());

  return 0;
}
