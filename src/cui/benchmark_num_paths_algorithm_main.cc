#include "multi_cost_graph/multi_cost_graph.h"
#include "multi_cost_graph/dijkstra.h"
#include "regret/regret.h"
#include "shortest_path_oracle/shortest_path_oracle.h"
#include "representative_paths/representative_paths.h"
#include "utility/graph_coordinate.h"
#include "utility/functions.h"
#include <gflags/gflags.h>
using namespace multi_cost_graph;

DEFINE_string(graph_file, "data/USA-road.NY.mcg4", "");
DEFINE_string(coordinate_file, "data/USA-road.NY.co", "");
DEFINE_string(num_paths, "1,2,4,8,16", "");

DEFINE_string(algorithms, "regret_minimizing,top_k,random", "");
DEFINE_double(distance_lb, 1000, "");
DEFINE_double(distance_ub, 10000, "");
DEFINE_int32(num_pairs, 10, "");

DEFINE_bool(skip_regret_evaluation, false, "");

int main(int argc, char **argv) {
  JLOG_INIT(&argc, argv);
  google::ParseCommandLineFlags(&argc, &argv, true);
  common::Random random;

  multi_cost_graph::Graph graph;
  utility::GraphCoordinate coordinate;
  graph.Read(FLAGS_graph_file.c_str());
  coordinate.Read(FLAGS_coordinate_file.c_str());
  CHECK(graph.NumVertices() == coordinate.NumVertices());

  shared_ptr<shortest_path_oracle::OracleInterface>
  oracle(shortest_path_oracle::ShortestPathOracleFromName("bi_astar", graph));

  JLOG_OPEN("setting") {
    JLOG_PUT("graph_file", FLAGS_graph_file);
    JLOG_PUT("num_paths", FLAGS_num_paths);
    JLOG_PUT("algorithms", FLAGS_algorithms);

    JLOG_PUT("distance_lb", FLAGS_distance_lb);
    JLOG_PUT("distance_ub", FLAGS_distance_ub);
  }

  vector<pair<VertexType, VertexType>> pairs(FLAGS_num_pairs);
  coordinate.ResetRandom();
  for (int i = 0; i < FLAGS_num_pairs; ++i) {
    pairs[i] = coordinate.RandomReachableVertexPairWithinDistanceRange
        (FLAGS_distance_lb, FLAGS_distance_ub, oracle);
  }

  const vector<string> algorithms =
      utility::ParseCommaSeparatedString<string>(FLAGS_algorithms);

  const vector<int> num_paths =
      utility::ParseCommaSeparatedString<int>(FLAGS_num_paths);

  for (string a : algorithms) {
    JLOG_ADD_OPEN("algorithm") {
      unique_ptr<representative_paths::RepresentativePathsInterface>
      algorithm(representative_paths::RepresentativePathsFromName(a, graph));

      JLOG_PUT("representative_paths_name", algorithm->RepresentativePathsName());
      JLOG_PUT("algorithm_name", algorithm->AlgorithmName());

      JLOG_PUT_BENCHMARK("time_precompute") {
        algorithm->Precompute();
      }

      for (int k : num_paths) {
        JLOG_ADD_OPEN("case") {
          JLOG_PUT("num_paths", k);

          for (int p = 0; p < FLAGS_num_pairs; ++p) {
            JLOG_ADD_OPEN("pair") {
              JLOG_PUT("v_from", pairs[p].first);
              JLOG_PUT("v_to", pairs[p].second);
              vector<Path> paths;
              algorithm->ResetStatistics();
              JLOG_PUT_BENCHMARK("time") {
                paths = algorithm->Query(pairs[p].first, pairs[p].second, k);
              }
              algorithm->OutputStatistics();

              for (auto &path : paths) {
                assert(path.From() == pairs[p].first);
                assert(path.To() == pairs[p].second);
              }

              if (FLAGS_skip_regret_evaluation) continue;
              {
                oracle->ResetQueryPoints(pairs[p].first, pairs[p].second);
                regret::RegretQuery q(oracle, paths);
                JLOG_PUT("regret", regret::RegretExact(q, nullptr));
              }
            }
          }
        }
      }
    }
  }

  exit(EXIT_SUCCESS);
}
