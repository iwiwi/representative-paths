#include "multi_cost_graph/multi_cost_graph.h"
#include "common/common.h"
#include <gflags/gflags.h>
using namespace multi_cost_graph;

DEFINE_string(graph_file, "", "");

int main(int argc, char **argv) {
  google::ParseCommandLineFlags(&argc, &argv, true);

  Graph g;
  g.Read(FLAGS_graph_file.c_str());
  g.PrintTSV(cout);
  return 0;
}
