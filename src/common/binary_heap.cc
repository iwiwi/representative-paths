#include "binary_heap.h"

constexpr BinaryHeap::KeyType BinaryHeap::kInfinityKey;
constexpr BinaryHeap::ValueType BinaryHeap::kInfinityValue;
