#pragma once
#include "common/common.h"

class BinaryHeap {
 public:
  typedef SizeType KeyType;
  typedef WeightType ValueType;
  typedef pair<KeyType, ValueType> EntryType;

  BinaryHeap(SizeType size) :
    size_(size), heap_size_(0),
    heap_(new EntryType[size + 1]), position_(new KeyType[size]) {
    fill(position_.get(), position_.get() + size, kInfinityKey);
  }

  inline bool IsEmpty() const {
    return heap_size_ == 0;
  }

  inline void Reset() {
    for (SizeType i = 0; i < heap_size_; ++i) {
      const auto &e = heap_[i];
      position_[e.first] = kInfinityKey;
    }
    heap_size_ = 0;
  }

  inline void Update(KeyType key, ValueType value) {
    if (position_[key] == kInfinityKey) {
      heap_[position_[key] = heap_size_++] = make_pair(key, value);
    } else {
      if (value > heap_[position_[key]].second) return;
      heap_[position_[key]].second = value;
    }
    UpHeap(position_[key]);
  }

  inline EntryType Top() const {
    return heap_[0];
  }

  inline EntryType Pop() {
    const EntryType e = heap_[0];
    position_[e.first] = kInfinityKey;
    if (heap_size_-- > 1) {
      Move(heap_size_, 0);
      DownHeap(0);
    }
    return e;
  }

  void Print() const {
    cout << "--" << endl;
    PrintRecursive(0, 0);
    cout << "--" << endl;
  }

 private:
  static constexpr KeyType kInfinityKey = numeric_limits<KeyType>::max();
  static constexpr ValueType kInfinityValue = numeric_limits<ValueType>::max();

  const SizeType size_;
  SizeType heap_size_;
  unique_ptr<EntryType[]> heap_;
  unique_ptr<KeyType[]> position_;

  inline void Set(SizeType k, const EntryType &e) {
    position_[e.first] = k;
    heap_[k] = e;
  }

  inline void Move(SizeType from, SizeType to) {
    position_[heap_[from].first] = to;
    heap_[to] = heap_[from];
  }

  inline void UpHeap(SizeType k) {
    const EntryType e = heap_[k];
    while (k > 0 && heap_[(k - 1) / 2].second > e.second) {
      Move((k - 1) / 2, k);
      k = (k - 1) / 2;
    }
    Set(k, e);
  }

  inline void DownHeap(SizeType k) {
    const EntryType e = heap_[k];
    while (k * 2 + 1 < heap_size_) {
      const SizeType c1 = k * 2 + 1;
      const SizeType c2 = c1 + 1;
      const SizeType c =
          (c2 < heap_size_ && heap_[c1].second > heap_[c2].second ? c2 : c1);

      if (e.second <= heap_[c].second) break;
      Move(c, k);
      k = c;
    }
    Set(k, e);
  }

  void PrintRecursive(SizeType k, SizeType depth) const {
    if (k >= heap_size_) return;
    PrintRecursive(k * 2 + 1, depth + 1);
    cout << string(depth, ' ') << heap_[k].first << ":" << heap_[k].second << endl;
    PrintRecursive(k * 2 + 2, depth + 1);
  }
};
