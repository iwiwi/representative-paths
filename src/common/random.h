#include <array>
#include <gflags/gflags.h>

DECLARE_int64(random_seed);

namespace common {
// http://xorshift.di.unimi.it/xorshift64star.c
class XorShift64Star {
 public:
  XorShift64Star(uint64_t seed = FLAGS_random_seed) : x(seed) {}

  uint64_t Next() {
    x ^= x >> 12;
    x ^= x << 25;
    x ^= x >> 27;
    return x * 2685821657736338717LL;
  }

  // [0, limit)
  uint64_t Next(uint64_t limit) {
    return Next() % limit;
  }

 private:
  uint64_t x;
};

// http://xorshift.di.unimi.it/xorshift1024star.c
class XorShift1024Star {
 public:
  XorShift1024Star(uint64_t seed = FLAGS_random_seed) : p(0) {
    XorShift64Star x(seed);
    for (int i = 0; i < 16; ++i) s[i] = x.Next();
  }

  uint64_t Next() {
    uint64_t s0 = s[ p ];
    uint64_t s1 = s[ p = ( p + 1 ) & 15 ];
    s1 ^= s1 << 31; // a
    s1 ^= s1 >> 11; // b
    s0 ^= s0 >> 30; // c
    return ( s[ p ] = s0 ^ s1 ) * 1181783497276652981LL;
  }

  // [0, limit)
  uint64_t Next(uint64_t limit) {
    return Next() % limit;
  }

  static constexpr uint64_t Max() {
    return numeric_limits<uint64_t>::max();
  }

 private:
  int p;
  std::array<uint64_t, 16> s;
};

typedef XorShift1024Star Random;
}  // namespace common
