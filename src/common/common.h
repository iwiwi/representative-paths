#pragma once

#include <cstdint>
#include <limits>
#include <queue>
#include <cassert>
#include <memory>
#include <algorithm>
#include <iostream>
#include <climits>
#include <fstream>
#include <sstream>
#include <set>
#include <numeric>
#include <unordered_map>
#include <gflags/gflags.h>
using namespace std;

typedef uint64_t SizeType;
static constexpr SizeType kInfinitySize = numeric_limits<SizeType>::max();

#include "macros.h"
#include "weight_type.h"
#include "weight_vector.h"
#include "jlog.h"
#include "random.h"
