#include "test.h"
#include "common.h"
using namespace multi_cost_graph;

void ConstructRandomGraph(Graph *g) {
  const SizeType kNumBidirectionalRoads = 30;
  const SizeType kNumUnidirectionlRoads = 30;
  const SizeType kNumVertices = 10;
  const SizeType kNumDimensions = 3;

  vector<Graph::EdgeTupleType> edges;

  for (SizeType i = 0; i < kNumBidirectionalRoads; ++i) {
    VertexType u, v;
    do {
      u = rand() % kNumVertices;
      v = rand() % kNumVertices;
    } while (u == v);
    const WeightVector c = RandomWeightVectorRandomLength(kNumDimensions);

    edges.emplace_back(u, v, c);
    edges.emplace_back(v, u, c);
  }

  for (SizeType i = 0; i < kNumUnidirectionlRoads; ++i) {
    VertexType u, v;
    do {
      u = rand() % kNumVertices;
      v = rand() % kNumVertices;
    } while (u == v);
    const WeightVector c = RandomWeightVectorRandomLength(kNumDimensions);

    edges.emplace_back(u, v, c);
  }

  random_shuffle(all(edges));

  g->Construct(move(edges));
}
