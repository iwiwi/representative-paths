#include "binary_heap.h"
#include "common/common.h"
#include "gtest/gtest.h"

namespace {
static constexpr BinaryHeap::ValueType kInfinityValue = numeric_limits<BinaryHeap::ValueType>::max();

class BinaryHeapTester {
 public:
  typedef typename BinaryHeap::KeyType KeyType;
  typedef typename BinaryHeap::ValueType ValueType;

  BinaryHeapTester(SizeType size)
  : size_(size), bh_(size), value_(size, kInfinityValue) {}

  bool IsEmpty() {
    return bh_.IsEmpty();
  }

  void CheckIsEmpty() {
    const ValueType min_value = *min_element(all(value_));
    const bool is_empty = min_value == kInfinityValue;
    ASSERT_EQ(is_empty, bh_.IsEmpty());
  }

  void Update(KeyType key, ValueType value) {
    if (value > value_[key]) return;
    // cout << key << " " << value << endl;
    value_[key] = value;
    bh_.Update(key, value);
  }

  void Pop() {
    KeyType key = min_element(all(value_)) - value_.begin();
    ASSERT_EQ(make_pair(key, value_[key]), bh_.Pop());
    value_[key] = kInfinityValue;
  }

  void PopAll() {
    ASSERT_NO_FATAL_FAILURE(CheckIsEmpty());
    while (IsEmpty()) {
      ASSERT_NO_FATAL_FAILURE(CheckIsEmpty());
      Pop();
    }
  }

  void Reset() {
    bh_.Reset();
    value_.assign(size_, kInfinityValue);
  }

  void Print() {
    bh_.Print();
  }

 private:

  SizeType size_;
  BinaryHeap bh_;
  vector<ValueType> value_;
};
}  // namespace

TEST(BinaryHeap, Trivial) {
  BinaryHeapTester tester(5);
  ASSERT_NO_FATAL_FAILURE(tester.Update(3, 10));
  ASSERT_NO_FATAL_FAILURE(tester.Update(1, 5));
  ASSERT_NO_FATAL_FAILURE(tester.Update(0, 8));
  ASSERT_NO_FATAL_FAILURE(tester.Update(2, 9));
  ASSERT_NO_FATAL_FAILURE(tester.PopAll());
}

TEST(BinaryHeap, Random) {
  const SizeType kNumResets = 10;
  const SizeType kNumTrials = 300;
  const SizeType kNumElements = 300;

  BinaryHeapTester tester(kNumElements);
  for (SizeType reset = 0; reset < kNumResets; ++reset) {
    for (SizeType trial = 0; trial < kNumTrials; ++trial) {
      SizeType num_insertion = 1 + rand() % kNumElements;
      for (SizeType i = 0; i < num_insertion; ++i) {
        ASSERT_NO_FATAL_FAILURE(tester.Update(rand() % kNumElements, rand()));
      }
      ASSERT_NO_FATAL_FAILURE(tester.CheckIsEmpty());

      SizeType num_pop = rand() % num_insertion;
      for (SizeType i = 0; i < num_pop; ++i) {
        if (tester.IsEmpty()) break;
        ASSERT_NO_FATAL_FAILURE(tester.Pop());
        ASSERT_NO_FATAL_FAILURE(tester.CheckIsEmpty());
      }
    }

    tester.Reset();
    ASSERT_NO_FATAL_FAILURE(tester.CheckIsEmpty());
  }
}
