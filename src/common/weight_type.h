#pragma once

#include <limits>

typedef double WeightType;

static constexpr WeightType kInfinityWeight = numeric_limits<WeightType>::max();
static constexpr WeightType kEpsWeight = 1E-8;

inline WeightType Abs(WeightType x) {
  return fabs(x);
}

inline bool IsZero(WeightType x) {
  return fabs(x) < kEpsWeight;
}

inline int SigNum(WeightType x) {
  return IsZero(x) ? 0 : (x > 0 ? +1 : -1);
}

inline bool IsEqual(WeightType a, WeightType b) {
  return IsZero(b - a) || IsZero((b - a) / max(Abs(a), Abs(b)));
}

inline bool IsLessThan(WeightType a, WeightType b) {
  return IsEqual(a, b) ? false : (a < b);
}
