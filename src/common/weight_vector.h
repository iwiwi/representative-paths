#include <vector>
#include <cassert>
#include "weight_type.h"
using namespace std;

typedef vector<WeightType> WeightVector;

inline WeightVector Add(WeightVector a, const WeightVector &b) {
  for (SizeType i = 0; i < a.size(); ++i) {
    a[i] += b[i];
  }
  return a;  // move
}

// |inline WeightVector Add(WeightVector, const WeightStorageType *)|
// is defined in |multi_cost_graph/types.h|

inline WeightVector Sub(WeightVector a, const WeightVector &b) {
  for (SizeType i = 0; i < a.size(); ++i) {
    a[i] -= b[i];
  }
  return a;  // move
}

inline WeightVector Mul(WeightVector a, WeightType scalar) {
  for (SizeType i = 0; i < a.size(); ++i) {
    a[i] *= scalar;
  }
  return a;
}

inline WeightType Dot(const WeightVector &a, const WeightVector &b) {
  assert(a.size() == b.size());
  WeightType w = 0.0;
  for (SizeType i = 0; i < a.size(); ++i) {
    w += a[i] * b[i];
  }
  return w;
}

inline WeightType Abs(const WeightVector &a) {
  return sqrt(Dot(a, a));
}

inline bool IsEqual(const WeightVector &a, const WeightVector &b) {
  for (SizeType i = 0; i < a.size(); ++i) {
    if (!IsEqual(a[i], b[i])) return false;
  }
  return true;
}

// a < b?
inline bool DoesDominate(const WeightVector &a, const WeightVector &b) {
  for (SizeType i = 0; i < a.size(); ++i) {
    if (IsLessThan(b[i], a[i])) return false;
  }
  return true;
}

inline WeightVector NormalUnitWeightVector(SizeType num_dimensions, SizeType nonzero_dimension) {
  WeightVector a(num_dimensions);
  a[nonzero_dimension] = 1.0;
  return a;
}

inline WeightVector RandomWeightVectorUnitLength(SizeType num_dimensions) {
  WeightVector a(num_dimensions);
  for (SizeType i = 0; i < num_dimensions; ++i) a[i] = rand() / WeightType(RAND_MAX);
  WeightType l = Abs(a);
  for (SizeType i = 0; i < num_dimensions; ++i) a[i] /= l;
  return a;
}

inline WeightVector RandomWeightVectorRandomLength(SizeType num_dimensions) {
  WeightVector a(num_dimensions);
  for (SizeType i = 0; i < num_dimensions; ++i) a[i] = rand() / WeightType(RAND_MAX);
  return a;
}

inline void PrintWeightVector(const WeightVector &a, ostream &os = cerr) {
  os << "(";
  for (SizeType i = 0; i < a.size(); ++i) {
    os << /*ios::fixed << setprecision(2) <<*/ a[i]
       << (i + 1 == a.size() ? ")" : ",");
  }
}
