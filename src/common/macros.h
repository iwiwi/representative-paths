#pragma once

#include <sys/time.h>
#include <unistd.h>
#include <stdarg.h>


#define CHECK(expr)                                                     \
  if (expr) {                                                           \
  } else {                                                              \
    fprintf(stderr, "CHECK Failed (%s:%d): %s\n",                       \
            __FILE__, __LINE__, #expr);                                 \
    exit(EXIT_FAILURE);                                                 \
  }

#define DISALLOW_COPY_AND_ASSIGN(TypeName)  \
  TypeName(const TypeName&);                \
  void operator=(const TypeName&)

#define all(c) (c).begin(), (c).end()

inline double CurrentTimeSec() {
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return tv.tv_sec + tv.tv_usec * 1e-6;
}

static constexpr double kQueryTimeout = 60 * 60 * 2;

// BENCHMARK macro
struct __bench__ {
  double start;
  char msg[100];
  __bench__(const char* format, ...)
  __attribute__((format(printf, 2, 3)))
  {
    va_list args;
    va_start(args, format);
    vsnprintf(msg, sizeof(msg), format, args);
    va_end(args);

    start = CurrentTimeSec();
  }
  ~__bench__() {
    fprintf(stderr, "%s: %.6f sec\n", msg, CurrentTimeSec() - start);
  }
  operator bool() { return false; }
};

#define BENCHMARK(...) if(__bench__ __b__ = __bench__(__VA_ARGS__));else

// TIMED macro
class __timer__ {
 public:
  __timer__(double &timer) : timer_(timer) {
    timer_ -= CurrentTimeSec();
  }

  ~__timer__(){
    timer_ += CurrentTimeSec();
  }

  operator bool() {
    return false;
  }

 private:
  double &timer_;
};

#define ACCUMULATE_TIME(timer)                            \
  if (__timer__ t__ = __timer__(timer)); else
