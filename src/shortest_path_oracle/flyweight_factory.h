#pragma once
#include "common/common.h"
#include "multi_cost_graph/multi_cost_graph.h"
using namespace std;

namespace shortest_path_oracle {
template<typename T>
class FlyweightFactory {
 public:
  static shared_ptr<T> Get(const multi_cost_graph::Graph &g) {
    SizeType h = g.Hash();
    auto r = pool_.insert(make_pair(h, shared_ptr<T>()));

    if (r.second) {
      r.first->second.reset(new T(g));
      JLOG_PUT_BENCHMARK("time_oracle_precompute_global") {
        r.first->second->PrecomputeGlobal();
      }
    }
    return r.first->second;
  }

  static void Clear() {
    pool_.clear();
  }

 private:
  static map<SizeType, shared_ptr<T>> pool_;
};

template<typename T>
map<SizeType, shared_ptr<T>> FlyweightFactory<T>::pool_;
}  // namespace shortest_path_oracle
