#include "interfaces.h"
#include "common/common.h"
using namespace multi_cost_graph;

namespace shortest_path_oracle {
OracleInterface::OracleInterface(const Graph &graph)
: graph_(graph), s_(multi_cost_graph::kDummyVertex), t_(multi_cost_graph::kDummyVertex),
  time_global_precomputation_(0.0), time_points_precomputation_(0.0),
  time_queries_(0.0), num_queries_(0), num_visited_vertices_(0) {
}

void OracleInterface::PrecomputeGlobal() {
  ACCUMULATE_TIME(time_global_precomputation_) {
    PrecomputeGlobalInternal();
  }
}

void OracleInterface::ResetQueryPoints(VertexType s, VertexType t) {
  s_ = s;
  t_ = t;

  time_points_precomputation_ = 0.0;
  time_queries_ = 0.0;
  num_queries_ = 0;

  ACCUMULATE_TIME(time_points_precomputation_) {
    PrecomputeQueryPointsInternal();
  }
}

WeightType OracleInterface::Query(const WeightVector &preference,
                                  multi_cost_graph::Path *shortest_path) {
  ++num_queries_;
  ACCUMULATE_TIME(time_queries_) {
    return QueryInternal(preference, shortest_path);
  }
  assert(false);
  return kInfinityWeight;
}

void OracleInterface::ResetStatistics() {
  time_global_precomputation_ = time_points_precomputation_ = time_queries_ = 0.0;
  num_queries_ = num_visited_vertices_ = 0;
}

void OracleInterface::OutputStatistics() const {
  JLOG_OPEN("shortest_path_oracle") {
    JLOG_PUT("name", Name());
    JLOG_PUT("time_global_precomputation", time_global_precomputation_);
    JLOG_PUT("time_points_precomputation", time_points_precomputation_);
    JLOG_PUT("time_queries", time_queries_);
    JLOG_PUT("num_queries", num_queries_);
    JLOG_PUT("num_visited_vertices", num_visited_vertices_);
  }
}

WeightType LazyOracleInterface::QueryInternal(const WeightVector &preference,
                                              Path *shortest_path) {
  shared_ptr<CoRoutineInterface> lazy_evaluator = Start(preference);
  while (!lazy_evaluator->IsDone()) lazy_evaluator->Advance();
  return lazy_evaluator->ShortestPath(shortest_path);
}
}  // namespace shortest_path_oracle
