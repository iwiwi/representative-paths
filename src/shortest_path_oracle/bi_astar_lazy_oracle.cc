#include "bi_astar_lazy_oracle.h"

#include "bi_astar_co_routine.h"
using namespace multi_cost_graph;

namespace shortest_path_oracle {
shared_ptr<CoRoutineInterface> BiAStarLazyOracle::Start(const WeightVector &preference) {
  return shared_ptr<CoRoutineInterface>(new BiAStarCoRoutine(*this, preference));
}
}  // namespace shortest_path_oracle
