#pragma once
#include "interfaces.h"
#include "shortest_path_oracle.h"
#include "common/common.h"
#include "multi_cost_graph/multi_cost_graph.h"

namespace shortest_path_oracle {
class BiDijkstraLazyOracle;

class BiDijkstraCoRoutine : public CoRoutineInterface {
 public:
  typedef multi_cost_graph::VertexType VertexType;

  virtual void Advance();
  virtual bool IsDone() const;
  virtual WeightType LowerBound() const;

  BiDijkstraCoRoutine(BiDijkstraLazyOracle &parent, const WeightVector &preference);

  virtual WeightType ShortestPath(multi_cost_graph::Path *shortest_path) const;

 private:
  struct VertexLabel {
    WeightType distance[2];
    VertexType parent[2];

    VertexLabel() {
      distance[0] = distance[1] = kInfinityWeight;
      parent[0] = parent[1] = multi_cost_graph::kDummyVertex;
    }

    VertexLabel(multi_cost_graph::DirectionType dir, WeightType d, VertexType p) {
      distance[dir] = d;
      parent[dir] = p;
      distance[multi_cost_graph::ReverseDirection(dir)] = kInfinityWeight;
      parent[multi_cost_graph::ReverseDirection(dir)] = multi_cost_graph::kDummyVertex;
    }

    WeightType DistanceSum() const {
      return (distance[0] == kInfinityWeight || distance[1] == kInfinityWeight) ? kInfinityWeight :
          distance[0] + distance[1];
    }
  };

  struct Hasher {
    SizeType operator()(VertexType v) const {
      return v * 1;  //9223372036854775837ULL + 1234567890123456789ULL;
    }
  };

  typedef priority_queue<pair<WeightType, VertexType>,
      vector<pair<WeightType, VertexType>>,
      greater<pair<WeightType, VertexType>>> PriorityQueueType;

  typedef unordered_map<VertexType, VertexLabel, Hasher> MapType;

  BiDijkstraLazyOracle &oracle_;
  const WeightVector preference_;

  WeightType lower_bound_;
  WeightType known_best_distance_;
  VertexType known_best_meeting_vertex_;

  PriorityQueueType queue_[2];
  MapType vertex_label_;

  void RestorePath(VertexType v, multi_cost_graph::DirectionType direction, multi_cost_graph::Path *path) const;
};

class BiDijkstraLazyOracle : public LazyOracleInterface {
 public:
  explicit BiDijkstraLazyOracle(const multi_cost_graph::Graph &graph) :
  LazyOracleInterface(graph) {}
  virtual ~BiDijkstraLazyOracle() {}

  shared_ptr<CoRoutineInterface> Start(const WeightVector &preference);

  virtual string Name() const {
    return "Bidirecitonal Dijkstra (lazy)";
  }

 protected:
  virtual void PrecomputeGlobalInternal() {}
  virtual void PrecomputeQueryPointsInternal() {}

 private:
  friend class BiDijkstraCoRoutine;
};
}  // namespace shortest_path_oracle
