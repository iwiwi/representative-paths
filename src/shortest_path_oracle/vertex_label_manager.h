#pragma once
#include "common/common.h"
#include "multi_cost_graph/multi_cost_graph.h"

namespace shortest_path_oracle {
template<typename VertexLabelType>
class VertexLabelManager;

class VertexLabelManagerParent {
 public:
  typedef multi_cost_graph::VertexType VertexType;

  explicit VertexLabelManagerParent(VertexType num_vs) :
      num_vs_(num_vs), num_ids_(0), id_(num_vs, multi_cost_graph::kDummyVertex) {}

  void Reset() {
    num_ids_ = 0;
    id_.assign(num_vs_, multi_cost_graph::kDummyVertex);
  }

  template<typename VertexLabelType>
  void Start(VertexLabelManager<VertexLabelType> *vertex_label_manager);

  inline VertexType GetID(VertexType v) const {
    return id_[v];
  }

  inline VertexType GetOrCreateID(VertexType v) {
    return id_[v] == multi_cost_graph::kDummyVertex ? (id_[v] = num_ids_++) : id_[v];
  }

  inline VertexType Count() const {
    return num_ids_;
  }

 private:
  VertexType num_vs_, num_ids_;
  vector<VertexType> id_;
};

template<typename VertexLabelType>
class VertexLabelManager {
 public:
  typedef multi_cost_graph::VertexType VertexType;

  VertexLabelManager() : parent_(NULL) {}

  VertexLabelType &Get(VertexType v) {
    const VertexType id = parent_->GetOrCreateID(v);
    if (vertex_labels_.size() <= id) {
      vertex_labels_.resize((id + 1) * 2);
    }
    return vertex_labels_[id];
  }

  const VertexLabelType &Get(VertexType v) const {
    const VertexType id = parent_->GetOrCreateID(v);
    return id < vertex_labels_.size() ? vertex_labels_[id] : dummy_vertex_label_;
  }

 private:
  friend class VertexLabelManagerParent;

  VertexLabelManagerParent *parent_;
  vector<VertexLabelType> vertex_labels_;
  VertexLabelType dummy_vertex_label_;
};

template<typename VertexLabelType>
void VertexLabelManagerParent::Start(VertexLabelManager<VertexLabelType> *vertex_label_manager) {
  vertex_label_manager->parent_ = this;
  vertex_label_manager->vertex_labels_.clear();
}
}  // namespace shortest_path_oracle
