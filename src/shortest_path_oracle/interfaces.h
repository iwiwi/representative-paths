#pragma once

#include "multi_cost_graph/multi_cost_graph.h"
#include "common/common.h"
#include "common/binary_heap.h"

namespace shortest_path_oracle {
class OracleInterface {
 public:
  explicit OracleInterface(const multi_cost_graph::Graph &graph);
  virtual ~OracleInterface() {}

  void PrecomputeGlobal();
  void ResetQueryPoints(multi_cost_graph::VertexType s, multi_cost_graph::VertexType t);
  WeightType Query(const WeightVector &preference, multi_cost_graph::Path *shortest_path);

  inline SizeType NumDimensions() {
    return graph_.NumDimensions();
  }

  virtual void OutputStatistics() const;
  virtual void ResetStatistics();

  virtual string Name() const = 0;

 protected:
  const multi_cost_graph::Graph &graph_;
  multi_cost_graph::VertexType s_, t_;

  // Statistics
  double time_global_precomputation_;
  double time_points_precomputation_;
  double time_queries_;
  SizeType num_queries_;
  SizeType num_visited_vertices_;

  // Implement the following functions
  virtual void PrecomputeGlobalInternal() = 0;
  virtual void PrecomputeQueryPointsInternal() = 0;
  virtual WeightType QueryInternal(const WeightVector&, multi_cost_graph::Path*) = 0;
};

class CoRoutineInterface {
 public:
  CoRoutineInterface() {}
  virtual ~CoRoutineInterface() {}

  // Implement the following functions
  virtual void Advance() = 0;
  virtual bool IsDone() const = 0;
  virtual WeightType LowerBound() const = 0;
  virtual WeightType ShortestPath(multi_cost_graph::Path *shortest_path) const = 0;
};

class LazyOracleInterface : public OracleInterface {
 public:
  explicit LazyOracleInterface(const multi_cost_graph::Graph &graph) : OracleInterface(graph) {}
  virtual ~LazyOracleInterface() {}

  // Implement this function
  virtual shared_ptr<CoRoutineInterface> Start(const WeightVector &preference) = 0;

 protected:
  virtual void PrecomputeGlobalInternal() = 0;
  virtual void PrecomputeQueryPointsInternal() = 0;

  virtual WeightType QueryInternal(const WeightVector &preference, multi_cost_graph::Path *shortest_path);
};
}  // namespace shrotest_path_oracle
