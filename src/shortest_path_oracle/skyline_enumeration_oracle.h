#pragma once
#include "multi_cost_graph/multi_cost_graph.h"
#include "representative_paths/skyline_paths.h"
#include "common/common.h"
#include "common/binary_heap.h"
#include "interfaces.h"

namespace shortest_path_oracle {
class SkylineEnumerationOracle : public OracleInterface {
 public:
  SkylineEnumerationOracle(const multi_cost_graph::Graph &graph) :
    OracleInterface(graph), skyline_enumerator_(graph) {}

  virtual ~SkylineEnumerationOracle() {}

  virtual string Name() const {
    return "SkylineEnumeration";
  }

 protected:
  representative_paths::SkylinePaths skyline_enumerator_;
  vector<multi_cost_graph::Path> skyline_paths_;

  virtual void PrecomputeGlobalInternal() {
    skyline_enumerator_.Precompute();
  }

  virtual void PrecomputeQueryPointsInternal() {
    skyline_paths_ = skyline_enumerator_.Query(s_, t_, 0);
  }

  virtual WeightType QueryInternal(const WeightVector& w, multi_cost_graph::Path* p) {
    WeightType c = kInfinityWeight;
    for (const auto &tp : skyline_paths_) {
      WeightType tc = tp.PersonalizedCost(w);
      if (tc < c) {
        c = tc;
        if (p != nullptr) *p = tp;
      }
    }
    return c;
  }
};
}  // namespace shortest_path_oracle
