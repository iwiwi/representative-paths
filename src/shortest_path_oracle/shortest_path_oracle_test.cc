#include "gtest/gtest.h"
#include "shortest_path_oracle.h"
#include "common/common.h"
#include "common/test.h"
using namespace shortest_path_oracle;
using namespace multi_cost_graph;

TEST(ShortestPathOracle, OracleRandom) {
  const SizeType kNumGraphs = 100;
  const SizeType kNumPairs = 10;
  const SizeType kNumPreferences = 10;

  for (SizeType i = 0; i < kNumGraphs; ++i) {
    Graph g;
    ConstructRandomGraph(&g);

    vector<unique_ptr<OracleInterface>> oracles;
    oracles.emplace_back(new DijkstraOracle(g));
    oracles.emplace_back(new BiDijkstraLazyOracle(g));
    oracles.emplace_back(new BiAStarLazyOracle(g));

    for (const auto &o : oracles) o->PrecomputeGlobal();

    for (SizeType j = 0; j < kNumPairs; ++j) {
      const VertexType s = rand() % g.NumVertices();
      const VertexType t = rand() % g.NumVertices();
      for (const auto &o : oracles) o->ResetQueryPoints(s, t);


      for (SizeType k = 0; k < kNumPreferences; ++k) {
        const WeightVector preference = RandomWeightVectorUnitLength(g.NumDimensions());

        Path path0;
        WeightType distance0 = oracles[0]->Query(preference, &path0);
        ASSERT_TRUE(IsEqual(distance0, path0.PersonalizedCost(preference)));

        for (const auto &o : oracles) {
          Path path1;
          WeightType distance1 = o->Query(preference, &path1);
          ASSERT_TRUE(IsEqual(distance0, distance1));

          if (distance0 != kInfinityWeight) {
            ASSERT_EQ(s, path1.From());
            ASSERT_EQ(t, path1.To());
            ASSERT_TRUE(IsEqual(distance0, path1.PersonalizedCost(preference)));
          }
        }
      }
    }

    for (const auto &o : oracles) o->OutputStatistics();
  }
}
