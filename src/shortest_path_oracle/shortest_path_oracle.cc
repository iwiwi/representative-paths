#include "shortest_path_oracle.h"
#include "multi_cost_graph/multi_cost_graph.h"
#include "flyweight_factory.h"
using namespace multi_cost_graph;

DEFINE_string(shortest_path_oracle, "bi_astar", "");

namespace shortest_path_oracle {
shared_ptr<OracleInterface> ShortestPathOracle(const Graph &graph) {
  return ShortestPathOracleFromName(FLAGS_shortest_path_oracle, graph);
}

shared_ptr<OracleInterface> ShortestPathOracleFromName(const string &name, const Graph &graph) {
  if (name == "dijkstra") {
    return FlyweightFactory<DijkstraOracle>::Get(graph);
  } else if (name == "bi_dijkstra") {
    return FlyweightFactory<BiDijkstraLazyOracle>::Get(graph);
  } else if (name == "bi_astar") {
    return FlyweightFactory<BiAStarLazyOracle>::Get(graph);
  } else if (name == "skyline") {
    return FlyweightFactory<SkylineEnumerationOracle>::Get(graph);
  } else {
    CHECK(!"Unknown oracle name specified");
    return nullptr;
  }
}
}  // namesapce shortest_path_oracle
