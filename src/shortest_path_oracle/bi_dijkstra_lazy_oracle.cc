#include "bi_dijkstra_lazy_oracle.h"
using namespace multi_cost_graph;

namespace shortest_path_oracle {
BiDijkstraCoRoutine::BiDijkstraCoRoutine(
    BiDijkstraLazyOracle &parent, const WeightVector &preference) :
            oracle_(parent), preference_(preference),
            lower_bound_(0.0),
            known_best_distance_(kInfinityWeight), known_best_meeting_vertex_(kDummyVertex) {
  vertex_label_.reserve(100000);
  vertex_label_.insert(make_pair(oracle_.s_, VertexLabel(kDirectionForward, 0.0, kDummyVertex)));
  vertex_label_.insert(make_pair(oracle_.t_, VertexLabel(kDirectionBackward, 0.0, kDummyVertex)));

  if (oracle_.s_ == oracle_.t_) {
    known_best_distance_ = 0.0;
    known_best_meeting_vertex_ = oracle_.s_;
  } else {
    queue_[kDirectionForward ].emplace(0.0, oracle_.s_);
    queue_[kDirectionBackward].emplace(0.0, oracle_.t_);
  }
}

bool BiDijkstraCoRoutine::IsDone() const {
  return queue_[kDirectionForward].empty() && queue_[kDirectionBackward].empty();
}

WeightType BiDijkstraCoRoutine::LowerBound() const {
  return lower_bound_;
}

void BiDijkstraCoRoutine::Advance() {
  const WeightType df = queue_[kDirectionForward].empty() ? kInfinityWeight
      : queue_[kDirectionForward].top().first;
  const WeightType db = queue_[kDirectionBackward].empty() ? kInfinityWeight
      : queue_[kDirectionBackward].top().first;
  const DirectionType dir = df < db ? kDirectionForward : kDirectionBackward;
  if (queue_[dir].empty()) return;

  const WeightType dis = queue_[dir].top().first;

  if (IsLessThan(known_best_distance_, dis * 2)) {
    // Done
    lower_bound_ = known_best_distance_;
    queue_[kDirectionForward] = PriorityQueueType();
    queue_[kDirectionBackward] = PriorityQueueType();
    return;
  }

  const VertexType v = queue_[dir].top().second;
  queue_[dir].pop();
  if (dis > vertex_label_[v].distance[dir]) return;

  ++oracle_.num_visited_vertices_;

  for (SizeType i = 0; i < oracle_.graph_.Degree(v); ++i) {
    const Edge &e = oracle_.graph_.EdgeFrom(v, i);
    if (!e.HasDirection(dir)) continue;

    const VertexType tv = e.To();
    const WeightType td = dis + e.PersonalizedCost(preference_);
    assert(!IsLessThan(td, dis));

    auto ins = vertex_label_.insert(make_pair(tv, VertexLabel(dir, td, v)));
    VertexLabel &label = ins.first->second;
    { // Update
      if (!ins.second) {
        if (!IsLessThan(td, label.distance[dir])) continue;
        label.distance[dir] = td;
        label.parent[dir] = v;
      }
      queue_[dir].emplace(td, tv);
    }
    { // Meet?
      if (IsLessThan(label.DistanceSum(), known_best_distance_)) {
        known_best_distance_ = label.DistanceSum();
        known_best_meeting_vertex_ = tv;
      }
    }
  }

  lower_bound_ = dis * 2;
}

void BiDijkstraCoRoutine::RestorePath(VertexType from, DirectionType direction,
                                          multi_cost_graph::Path *path) const {
  WeightVector cost(oracle_.NumDimensions());
  vector<VertexType> vertices;

  for (VertexType tv = from; ; ) {
    vertices.emplace_back(tv);

    const VertexLabel &label1 = vertex_label_.find(tv)->second;
    const WeightType td = label1.distance[direction];
    const VertexType v = label1.parent[direction];
    if (v == kDummyVertex) break;

    const VertexLabel &label2 = vertex_label_.find(v)->second;
    const WeightType d = label2.distance[direction];

    for (SizeType i = 0; i < oracle_.graph_.Degree(v); ++i) {
      const Edge &e = oracle_.graph_.EdgeFrom(v, i);
      if (!e.HasDirection(direction)) continue;
      if (e.To() == tv && IsEqual(td, d + e.PersonalizedCost(preference_))) {
        cost = e.AddCost(cost);
        tv = v;
        break;
      }
    }
    assert(tv == v);
  }

  *path = Path(vertices, cost);
}

WeightType BiDijkstraCoRoutine::ShortestPath(multi_cost_graph::Path *shortest_path) const {
  if (known_best_distance_ == kInfinityWeight) {
    *shortest_path = Path();
    return kInfinityWeight;
  }

  if (shortest_path == nullptr) {
    return known_best_distance_;
  }

  // Forward
  RestorePath(known_best_meeting_vertex_, kDirectionForward, shortest_path);
  shortest_path->Reverse();

  // Backward
  {
    Path p;
    RestorePath(known_best_meeting_vertex_, kDirectionBackward, &p);
    shortest_path->Concatenate(p);
  }

  assert(IsEqual(shortest_path->PersonalizedCost(preference_), known_best_distance_));
  return known_best_distance_;
}

shared_ptr<CoRoutineInterface> BiDijkstraLazyOracle::Start(const WeightVector &preference) {
  return shared_ptr<CoRoutineInterface>(new BiDijkstraCoRoutine(*this, preference));
}
}  // namespace shortest_path_oracle
