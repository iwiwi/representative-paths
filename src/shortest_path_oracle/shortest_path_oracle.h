#pragma once

#include "multi_cost_graph/multi_cost_graph.h"
#include "interfaces.h"
#include "dijkstra_oracle.h"
#include "bi_dijkstra_lazy_oracle.h"
#include "bi_astar_lazy_oracle.h"
#include "skyline_enumeration_oracle.h"
#include "common/common.h"

DECLARE_string(shortest_path_oracle);

namespace shortest_path_oracle {
// Reads |--shortest_path_oracle| flag (variable |FLAGS_shortest_path_oracle|)
shared_ptr<OracleInterface> ShortestPathOracle(const multi_cost_graph::Graph &graph);

shared_ptr<OracleInterface> ShortestPathOracleFromName(const string &name, const multi_cost_graph::Graph &graph);
}  // namespace shortest_path_oracle
