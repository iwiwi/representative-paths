#pragma once
#include "interfaces.h"
#include "shortest_path_oracle.h"
#include "vertex_label_manager.h"
#include "common/common.h"
#include "multi_cost_graph/multi_cost_graph.h"
#include "multi_cost_graph/alt_lower_bound.h"

namespace shortest_path_oracle {
class BiAStarLazyOracle;

class BiAStarLazyOracle : public LazyOracleInterface {
 public:
  typedef multi_cost_graph::VertexType VertexType;

  explicit BiAStarLazyOracle(const multi_cost_graph::Graph &graph)
  : LazyOracleInterface(graph), vertex_label_manager_parent_(graph.NumVertices()) {}

  virtual ~BiAStarLazyOracle() {}

  shared_ptr<CoRoutineInterface> Start(const WeightVector &preference);

  virtual WeightType LowerBound(VertexType v,
                                const WeightVector &preference,
                                multi_cost_graph::DirectionType dir) {
    vector<WeightType> &ulb = vertex_label_manager_.Get(v).unit_lower_bound[dir];

    if (ulb.empty()) {
      // This is the first time to access this vertex label
      const VertexType s = (dir == multi_cost_graph::kDirectionForward ? v : s_);
      const VertexType t = (dir == multi_cost_graph::kDirectionForward ? t_ : v);
      ulb.resize(graph_.NumDimensions());
      for (SizeType d = 0; d < graph_.NumDimensions(); ++d) {
        ulb[d] = alt_.LowerBound(s, t, d);
      }
    }

    WeightType lb = 0.0;
    for (SizeType d = 0; d < graph_.NumDimensions(); ++d) {
      const WeightType x = ulb[d];
      if (x == kInfinityWeight) return kInfinityWeight;
      lb += x * preference[d];
    }
    return lb;
  }

  virtual string Name() const {
    return "Bidirectional A* search (Lazy)";
  }

  SizeType VertexLabelSize() const {
    return vertex_label_manager_parent_.Count();
  }

 protected:
  // Global vertex label
  struct OracleVertexLabel {
    // unit_lower_bound[d] := Cost lower bound against unit normal vector |e_d|
    vector<WeightType> unit_lower_bound[2];
  };

  multi_cost_graph::ALTLowerBound alt_;
  VertexLabelManagerParent vertex_label_manager_parent_;
  VertexLabelManager<OracleVertexLabel> vertex_label_manager_;

  virtual void PrecomputeGlobalInternal() {
    alt_.PrepareLandmarks(graph_, multi_cost_graph::kDirectionForward);
  }

  virtual void PrecomputeQueryPointsInternal() {
    vertex_label_manager_parent_.Reset();
    vertex_label_manager_parent_.Start(&vertex_label_manager_);
  }

 private:
  friend class BiAStarCoRoutine;
};
}  // namespace shortest_path_oracle
