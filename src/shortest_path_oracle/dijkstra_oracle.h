#pragma once
#include "multi_cost_graph/multi_cost_graph.h"
#include "common/common.h"
#include "common/binary_heap.h"
#include "interfaces.h"

namespace shortest_path_oracle {
class DijkstraOracle : public OracleInterface {
 public:
  DijkstraOracle(const multi_cost_graph::Graph &graph) :
    OracleInterface(graph), que_(graph.NumVertices()) {}
  virtual ~DijkstraOracle() {}

  virtual string Name() const {
    return "Dijkstra";
  }

 protected:
  virtual void PrecomputeQueryPointsInternal() {}
  virtual void PrecomputeGlobalInternal() {}
  virtual WeightType QueryInternal(const WeightVector&, multi_cost_graph::Path*);

  BinaryHeap que_;
};
}  // namespace shortest_path_oracle
