#include "bi_astar_lazy_oracle.h"
#include "bi_astar_co_routine.h"
using namespace multi_cost_graph;

namespace shortest_path_oracle {
///////////////////////////////////////////////////////////////////////////////
// Vertex Label
///////////////////////////////////////////////////////////////////////////////

BiAStarCoRoutine::VertexLabel::VertexLabel(DirectionType dir, WeightType d, VertexType p) {
  distance[dir] = d;
  parent[dir] = p;
  distance[multi_cost_graph::ReverseDirection(dir)] = kInfinityWeight;
  parent[multi_cost_graph::ReverseDirection(dir)] = multi_cost_graph::kDummyVertex;
}

void BiAStarCoRoutine::VertexLabel::InitLowerBounds(BiAStarCoRoutine &evaluator, VertexType v) {
  auto &oracle = evaluator.oracle_;
  lower_bound[kDirectionForward] = oracle.LowerBound(v, evaluator.preference_, kDirectionForward);
  lower_bound[kDirectionBackward] = oracle.LowerBound(v, evaluator.preference_, kDirectionBackward);
}

///////////////////////////////////////////////////////////////////////////////
// CoRoutine
///////////////////////////////////////////////////////////////////////////////
BiAStarCoRoutine::BiAStarCoRoutine(
    BiAStarLazyOracle &parent, const WeightVector &preference) :
            oracle_(parent), preference_(preference),
            lower_bound_(0.0),
            known_best_distance_(kInfinityWeight), known_best_meeting_vertex_(kDummyVertex) {
  if (s() == t()) {
    known_best_distance_ = 0.0;
    known_best_meeting_vertex_ = s();
    return;
  }

  oracle_.vertex_label_manager_parent_.Start(&vertex_label_manager_);
  VertexLabel &ls = vertex_label_manager_.Get(s());
  VertexLabel &lt = vertex_label_manager_.Get(t());
  ls = VertexLabel(kDirectionForward, 0.0, kDummyVertex);
  lt = VertexLabel(kDirectionBackward, 0.0, kDummyVertex);
  ls.InitLowerBounds(*this, s());
  lt.InitLowerBounds(*this, t());

  queue_[kDirectionForward ].emplace(ls.Potential(kDirectionForward), s());
  queue_[kDirectionBackward].emplace(lt.Potential(kDirectionBackward), t());

  lower_bound_ = oracle_.LowerBound(s(), preference, kDirectionForward);
}

bool BiAStarCoRoutine::IsDone() const {
  return queue_[kDirectionForward].empty() && queue_[kDirectionBackward].empty();
}

WeightType BiAStarCoRoutine::LowerBound() const {
  return lower_bound_;
}

void BiAStarCoRoutine::Advance() {
  const WeightType df = queue_[kDirectionForward].empty() ? kInfinityWeight
      : queue_[kDirectionForward].top().first;
  const WeightType db = queue_[kDirectionBackward].empty() ? kInfinityWeight
      : queue_[kDirectionBackward].top().first;
  const DirectionType dir = df < db ? kDirectionForward : kDirectionBackward;
  if (queue_[dir].empty()) return;

  const WeightType p = queue_[dir].top().first;

  if (IsLessThan(known_best_distance_, p * 2)) {
    // Done
    lower_bound_ = known_best_distance_;
    queue_[kDirectionForward] = PriorityQueueType();
    queue_[kDirectionBackward] = PriorityQueueType();
    return;
  }

  const VertexType v = queue_[dir].top().second;
  queue_[dir].pop();
  const VertexLabel &l = vertex_label_manager_.Get(v);  // vertex_label_.find(v)->second;
  if (IsLessThan(l.Potential(dir), p)) return;
  const WeightType d = l.distance[dir];

  ++oracle_.num_visited_vertices_;

  for (SizeType i = 0; i < oracle_.graph_.Degree(v); ++i) {
    const Edge &e = oracle_.graph_.EdgeFrom(v, i);
    if (!e.HasDirection(dir)) continue;

    const VertexType tv = e.To();
    const WeightType td = d + e.PersonalizedCost(preference_);

    /*
    auto ins = vertex_label_.insert(make_pair(tv, VertexLabel(dir, td, v)));
    VertexLabel &tl = ins.first->second;
    if (ins.second) tl.InitLowerBounds(*this, tv);
    */
    VertexLabel &tl = vertex_label_manager_.Get(tv);

    if (tl.lower_bound[0] == -1.0) {  // New label
      tl = VertexLabel(dir, td, v);
      tl.InitLowerBounds(*this, tv);
    } else {
      if (!IsLessThan(td, tl.distance[dir])) continue;
      tl.distance[dir] = td;
      tl.parent[dir] = v;
    }
    queue_[dir].emplace(tl.Potential(dir), tv);

    { // Meet?
      if (IsLessThan(tl.DistanceSum(), known_best_distance_)) {
        known_best_distance_ = tl.DistanceSum();
        known_best_meeting_vertex_ = tv;
      }
    }
  }

  lower_bound_ = max(lower_bound_, p * 2);
}

void BiAStarCoRoutine::RestorePath(VertexType from, DirectionType direction,
                                          multi_cost_graph::Path *path) const {
  WeightVector cost(oracle_.NumDimensions());
  vector<VertexType> vertices;

  for (VertexType tv = from; ; ) {
    vertices.emplace_back(tv);

    const VertexLabel &label1 = vertex_label_manager_.Get(tv); // vertex_label_.find(tv)->second;
    const WeightType td = label1.distance[direction];
    const VertexType v = label1.parent[direction];
    if (v == kDummyVertex) break;

    const VertexLabel &label2 = vertex_label_manager_.Get(v); //vertex_label_.find(v)->second;
    const WeightType d = label2.distance[direction];

    for (SizeType i = 0; i < oracle_.graph_.Degree(v); ++i) {
      const Edge &e = oracle_.graph_.EdgeFrom(v, i);
      if (!e.HasDirection(direction)) continue;
      if (e.To() == tv && IsEqual(td, d + e.PersonalizedCost(preference_))) {
        cost = e.AddCost(cost);
        tv = v;
        break;
      }
    }
    assert(tv == v);
  }

  *path = Path(vertices, cost);
}

WeightType BiAStarCoRoutine::ShortestPath(multi_cost_graph::Path *shortest_path) const {
  if (s() == t()) {
    if (shortest_path != nullptr) *shortest_path = Path({s()}, WeightVector(oracle_.graph_.NumDimensions()));
    return 0.0;
  }

  PrintWeightVector(preference_);
  cout << "\t" << known_best_distance_ << "\t"
      << oracle_.LowerBound(s(), preference_, kDirectionForward) << "\t"
      << vertex_label_manager_.Get(s()).Potential(kDirectionForward) << endl;
      // << vertex_label_.at(s()).Potential(kDirectionForward) << endl;

  if (known_best_distance_ == kInfinityWeight) {
    *shortest_path = Path();
    return kInfinityWeight;
  }

  if (shortest_path == nullptr) {
    return known_best_distance_;
  }

  // Forward
  RestorePath(known_best_meeting_vertex_, kDirectionForward, shortest_path);
  shortest_path->Reverse();

  // Backward
  {
    Path p;
    RestorePath(known_best_meeting_vertex_, kDirectionBackward, &p);
    shortest_path->Concatenate(p);
  }

  assert(IsEqual(shortest_path->PersonalizedCost(preference_), known_best_distance_));
  return known_best_distance_;
}
}  // namespace shortest_path_oracle
