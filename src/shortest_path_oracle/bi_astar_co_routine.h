#pragma once
#include "interfaces.h"
#include "shortest_path_oracle.h"
#include "common/common.h"
#include "multi_cost_graph/multi_cost_graph.h"
#include "multi_cost_graph/alt_lower_bound.h"

namespace shortest_path_oracle {
class BiAStarCoRoutine : public CoRoutineInterface {
 public:
  typedef multi_cost_graph::VertexType VertexType;

  virtual void Advance();
  virtual bool IsDone() const;
  virtual WeightType LowerBound() const;

  BiAStarCoRoutine(BiAStarLazyOracle &parent, const WeightVector &preference);

  virtual WeightType ShortestPath(multi_cost_graph::Path *shortest_path) const;

 private:
  struct VertexLabel {
    WeightType distance[2];
    WeightType lower_bound[2];
    VertexType parent[2];

    VertexLabel() {
      lower_bound[0] = lower_bound[1] = -1.0;
    }
    VertexLabel(multi_cost_graph::DirectionType dir, WeightType d, VertexType p);

    void InitLowerBounds(BiAStarCoRoutine &evaluator, VertexType v);

    WeightType Potential(multi_cost_graph::DirectionType dir) const {
      return distance[dir] +
          (lower_bound[dir] - lower_bound[multi_cost_graph::ReverseDirection(dir)]) / 2.0;
    }

    WeightType DistanceSum() const {
      return (distance[0] == kInfinityWeight || distance[1] == kInfinityWeight) ? kInfinityWeight :
          distance[0] + distance[1];
    }
  };

  friend struct VertexLabel;

  typedef priority_queue<pair<WeightType, VertexType>,
      vector<pair<WeightType, VertexType>>,
      greater<pair<WeightType, VertexType>>> PriorityQueueType;

  BiAStarLazyOracle &oracle_;
  const WeightVector preference_;

  WeightType lower_bound_;
  WeightType known_best_distance_;
  VertexType known_best_meeting_vertex_;

  PriorityQueueType queue_[2];
  //MapType vertex_label_;
  VertexLabelManager<VertexLabel> vertex_label_manager_;

  inline VertexType s() const { return oracle_.s_; }
  inline VertexType t() const { return oracle_.t_; }

  void RestorePath(VertexType v, multi_cost_graph::DirectionType direction, multi_cost_graph::Path *path) const;
};
}  // namespace shortest_path_oracle
