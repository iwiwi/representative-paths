#include "dijkstra_oracle.h"
#include "common/binary_heap.h"
#include "common/common.h"
using namespace multi_cost_graph;

namespace shortest_path_oracle {
WeightType DijkstraOracle::QueryInternal(const WeightVector &preference, Path *path) {
  VertexType num_vs = graph_.NumVertices();
  vector<WeightType> pot(num_vs, kInfinityWeight);
  vector<pair<VertexType, const Edge*>> par(num_vs, make_pair(kDummyVertex, nullptr));

  que_.Reset();

  pot[s_] = 0;
  que_.Update(s_, 0);
  assert(!que_.IsEmpty());
  while (!que_.IsEmpty()) {
    VertexType v;
    WeightType d;
    tie(v, d) = que_.Pop();
    if (v == t_) break;

    ++num_visited_vertices_;

    for (SizeType i = 0; i < graph_.Degree(v); ++i) {
      const auto &e = graph_.EdgeFrom(v, i);
      if (!e.HasForward()) continue;

      WeightType td = d + e.PersonalizedCost(preference);
      VertexType tv = e.To();
      if (td < pot[tv]) {
        pot[tv] = td;
        par[tv] = make_pair(v, &e);
        que_.Update(tv, td);
      }
    }
  }

  if (pot[t_] != kInfinityWeight && path != nullptr) {
    vector<VertexType> vertices;
    WeightVector cost(graph_.NumDimensions());
    for (VertexType v = t_; v != s_; v = par[v].first) {
      vertices.emplace_back(v);
      for (SizeType d = 0; d < graph_.NumDimensions(); ++d) {
        cost[d] += par[v].second->Cost()[d];
      }
    }
    vertices.emplace_back(s_);
    reverse(all(vertices));
    *path = Path(vertices, cost);

    assert(path->From() == s_);
    assert(path->To() == t_);
    assert(IsEqual(path->PersonalizedCost(preference), pot[t_]));
  }

  return pot[t_];
}
}  // namespace shortest_path_oracle

