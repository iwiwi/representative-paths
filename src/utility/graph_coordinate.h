#pragma once
#include "multi_cost_graph/multi_cost_graph.h"
#include "shortest_path_oracle/shortest_path_oracle.h"
#include "common/common.h"

namespace utility {
class GraphCoordinate {
 public:
  typedef typename multi_cost_graph::VertexType VertexType;
  typedef double ValueType;

  void Read(const char *file);

  VertexType NumVertices() const {
    assert(longitude_.size() == latitude_.size());
    assert(longitude_.size() == height_.size());
    return longitude_.size();
  }

  ValueType Longitude(VertexType v) const {
    return longitude_[v];
  }

  ValueType Latitude(VertexType v) const {
    return latitude_[v];
  }

  ValueType Height(VertexType v) const {
    return height_[v];
  }

  // (longitude, latitude)
  pair<ValueType, ValueType> Coordinate(VertexType v) const {
    return std::make_pair(Longitude(v), Latitude(v));
  }

  // (x, y)
  pair<ValueType, ValueType> MercatorProjection(VertexType v) const {
    return make_pair(Longitude(v), atanh(sin(Latitude(v))));
  }

  // Estimated distance in meters by Hubeny's formula
  ValueType Distance(VertexType u, VertexType v) const;

  void ResetRandom() {
    random_ = decltype(random_)();
  }

  std::pair<VertexType, VertexType> RandomVertexPairWithinDistanceRange(
      ValueType distance_lb, ValueType distance_ub);

  std::pair<VertexType, VertexType> RandomReachableVertexPairWithinDistanceRange(
      ValueType distance_lb, ValueType distance_ub,
      shared_ptr<shortest_path_oracle::OracleInterface> spo);

 private:
  vector<ValueType> longitude_, latitude_;  // Expressed in radians
  vector<ValueType> height_;

  common::Random random_;
};
}
