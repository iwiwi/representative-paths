#pragma once
#include "common/common.h"
#include <complex>

DECLARE_double(resolution);

namespace utility {
struct Color {
  double r, g, b;

  string ToHEX() const {
    char buf[256];
    sprintf(buf, "#%02x%02x%02x",
            max(0, min(255, int(r * 256))),
            max(0, min(255, int(g * 256))),
            max(0, min(255, int(b * 256))));
    return buf;
  }

  // h, s, v \in [0, 1]
  static Color FromHSV(double h, double s, double v) {
    double r = v;
    double g = v;
    double b = v;

    h = fmod(fmod(h, 1.0) + 1.0, 1.0);
    if (s > 0.0f) {
      h *= 6.0f;
      int i = (int) h;
      double f = h - (double)i;
      switch (i) {
        default:
        case 0:
          g *= 1 - s * (1 - f);
          b *= 1 - s;
          break;
        case 1:
          r *= 1 - s * f;
          b *= 1 - s;
          break;
        case 2:
          r *= 1 - s;
          b *= 1 - s * (1 - f);
          break;
        case 3:
          r *= 1 - s;
          g *= 1 - s * f;
          break;
        case 4:
          r *= 1 - s * (1 - f);
          g *= 1 - s;
          break;
        case 5:
          g *= 1 - s;
          b *= 1 - s * f;
          break;
      }
    }

    return Color{r, g, b};
  }
};

class EPSImage {
 public:
  typedef complex<double> Point;

  void Open(const char *file) {
    ofs_.open(file);
    CHECK(ofs_);
  }

  void SetBoundingBox(double min_x, double max_x, double min_y, double max_y) {
    tie(min_x_, max_x_) = tie(min_x, max_x);
    tie(min_y_, max_y_) = tie(min_y, max_y);

    // Header
    ofs_ << "%!PS-Adobe-3.0 EPSF-3.0" << endl
        << "%%BoundingBox: " << 0 << " " << 0 << " "
        << (max_x_ - min_x_) * FLAGS_resolution << " "
        << (max_y_ - min_y_) * FLAGS_resolution << endl
        << "/m {moveto} def" << endl
        << "/l {lineto} def" << endl
        << "/s {stroke} def" << endl;
  }

  void SetLineWidth(double width) {
    ofs_ << width << " setlinewidth" << endl;
  }

  void SetColor(Color c) {
    SetColor(c.r, c.g, c.b);
  }

  void SetColor(double r, double g, double b) {
    cout << "Color: " << r << " " << g << " " << b << endl;
    ofs_ << r << " " << g << " " << b << " setrgbcolor" << endl;
  }

  void SetColorHSV(SizeType i, SizeType n) {
    double h = 0.1 + i / (double)n, s = 1.0, v = 0.9;

    double r = v;
    double g = v;
    double b = v;
    if (s > 0.0f) {
        h *= 6.0f;
        int i = (int) h;
        double f = h - (double)i;
        switch (i) {
            default:
            case 0:
                g *= 1 - s * (1 - f);
                b *= 1 - s;
                break;
            case 1:
                r *= 1 - s * f;
                b *= 1 - s;
                break;
            case 2:
                r *= 1 - s;
                b *= 1 - s * (1 - f);
                break;
            case 3:
                r *= 1 - s;
                g *= 1 - s * f;
                break;
            case 4:
                r *= 1 - s * (1 - f);
                g *= 1 - s;
                break;
            case 5:
                g *= 1 - s;
                b *= 1 - s * f;
                break;
        }
    }

    SetColor(r, g, b);
  }

  void MoveTo(const Point &p) {
    CHECK(ofs_);

    previous_point_ = p;
    previous_output_ = false;
  }

  void LineTo(const Point &p) {
    CHECK(ofs_);

    // TODO: more precise
    if (IsInside(previous_point_) && IsInside(p)) {
      if (!previous_output_) OutputPoint(previous_point_, "m");
      OutputPoint(p, "l");
      previous_output_ = true;
      waiting_stroke_ = true;
    } else {
      previous_point_ = false;
    }
    previous_point_ = p;
  }

  void Stroke() {
    CHECK(ofs_);

    if (waiting_stroke_) {
      ofs_ << "s" << endl;
      waiting_stroke_ = false;
    }
  }

  void Close() {
    ofs_.close();
  }

 private:
  ofstream ofs_;
  double min_x_, max_x_, min_y_, max_y_;

  Point previous_point_;
  bool previous_output_;
  bool waiting_stroke_;

  bool IsInside(const Point &p) {
    return
        min_x_ < p.real() && p.real() < max_x_ &&
        min_y_ < p.imag() && p.imag() < max_y_;
  }

  void OutputPoint(const Point &p, const char *command) {
    // TODO: OUTSIDE
    // double x = (p.real() - min_x_) / (max_x_ - min_x_) * FLAGS_eps_width;
    // double y = (p.imag() - min_y_) / (max_y_ - min_y_) * FLAGS_eps_height;
    double x = (p.real() - min_x_) * FLAGS_resolution;
    double y = (p.imag() - min_y_) * FLAGS_resolution;
    ofs_ << x << " " << y << " " << command << endl;;
  }
};
}  // namespace utility
