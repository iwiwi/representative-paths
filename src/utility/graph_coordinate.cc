#include "graph_coordinate.h"
namespace utility {
void GraphCoordinate::Read(const char *file) {
  ifstream ifs(file);
  CHECK(ifs);

  for (string line; getline(ifs, line); ) {
    if (line == "") continue;

    stringstream ss(line);
    double lng, lat;
    CHECK(ss >> lng >> lat);

    // TODO: purify the original data and remove
    lng = lng / 1000000.0 * (M_PI / 180.0);
    lat = lat / 1000000.0 * (M_PI / 180.0);

    longitude_.emplace_back(lng);
    latitude_.emplace_back(lat);

    double hei;
    if (ss >> hei) {
      height_.emplace_back(hei);
    }
  }

  CHECK(longitude_.size() == height_.size() || height_.empty());
  if (height_.empty()) {
    height_.assign(longitude_.size(), 0.0);
  }
}

// Copyright (c) 2013 Yamadarake
// Released under the MIT license
// http://opensource.org/licenses/mit-license.php
// (http://yamadarake.jp/trdi/report000001.html)
GraphCoordinate::ValueType GraphCoordinate::Distance(GraphCoordinate::VertexType u,
                                                     GraphCoordinate::VertexType v) const {
  static const double GRS80_A = 6378137.000;
  static const double GRS80_E2 = 0.00669438002301188;
  static const double GRS80_MNUM = 6335439.32708317;

  const double lat1 = latitude_[u], lng1 = longitude_[u];
  const double lat2 = latitude_[v], lng2 = longitude_[v];

  const double my = (lat1 + lat2) / 2.0;
  const double dy = lat1 - lat2;
  const double dx = lng1 - lng2;

  const double si = sin(my);
  const double w = sqrt(1.0 - GRS80_E2 * si * si);
  const double m = GRS80_MNUM / (w * w * w);
  const double n = GRS80_A / w;

  const double dym = dy * m;
  const double dxncos = dx * n * cos(my);

  return sqrt(dym * dym + dxncos * dxncos);
}

pair<GraphCoordinate::VertexType, GraphCoordinate::VertexType>
GraphCoordinate::RandomVertexPairWithinDistanceRange(
    GraphCoordinate::ValueType distance_lb,
    GraphCoordinate::ValueType distance_ub) {
  for (;;) {
    const VertexType s = random_.Next(NumVertices());
    const VertexType t = random_.Next(NumVertices());
    if (s == t) continue;
    const ValueType d = Distance(s, t);
    if (distance_lb <= d && d <= distance_ub) {
      return make_pair(s, t);
    }
  }

  // Just to satisfy eclipse...
  return make_pair(multi_cost_graph::kDummyVertex,
                   multi_cost_graph::kDummyVertex);
}

pair<GraphCoordinate::VertexType, GraphCoordinate::VertexType>
GraphCoordinate::RandomReachableVertexPairWithinDistanceRange(
    GraphCoordinate::ValueType distance_lb,
    GraphCoordinate::ValueType distance_ub,
    shared_ptr<shortest_path_oracle::OracleInterface> spo) {
  for (;;) {
    const pair<VertexType, VertexType> p =
        RandomVertexPairWithinDistanceRange(distance_lb, distance_ub);

    spo->ResetQueryPoints(p.first, p.second);
    const WeightType w =
        spo->Query(NormalUnitWeightVector(spo->NumDimensions(), 0), nullptr);
    if (w < kInfinityWeight) return p;
  }

  // Just to satisfy eclipse...
  return make_pair(multi_cost_graph::kDummyVertex,
                   multi_cost_graph::kDummyVertex);
}
}  // namespace utility
