#include "path_visualizer.h"
using namespace multi_cost_graph;

DEFINE_bool(crop, true, "");
DEFINE_bool(draw_background, false, "");
DEFINE_double(margin, 0.1, "");
DEFINE_double(aspect_ratio, 1.0, "0.75 = 4:3, -1.0 = unfixed aspect ratio");

namespace utility {
PathVisualizer::PathVisualizer(const multi_cost_graph::Graph& g, const GraphCoordinate& c)
: g_(g), ps_(g.NumVertices()) {
  for (VertexType v = 0; v < g.NumVertices(); ++v) {
    const double lon = c.Longitude(v);
    const double lat = c.Latitude(v);

    const double x = lon;
    const double y = atanh(sin(lat));

    ps_[v] = Point(x, y);
  }
}

void PathVisualizer::OutputAll(const vector<Path> &paths, const string &file_suffix) {
  OutputCostTSV(paths, file_suffix + ".tsv");
  OutputPathEPS(paths, file_suffix + ".eps");
}

void PathVisualizer::OutputCostTSV(const vector<multi_cost_graph::Path>& paths,
                                   const string& filename) {
  ofstream ofs(filename);
  CHECK(ofs);
  auto colors = GenerateColors(paths.size());
  for (SizeType i = 0; i < paths.size(); ++i) {
    const auto &p = paths[i];
    for (SizeType d = 0; d < g_.NumDimensions(); ++d) {
      ofs << p.Cost()[d] << "\t";
    }
    ofs << colors[i].ToHEX() << endl;
  }
}

void PathVisualizer::OutputPathEPS(const vector<multi_cost_graph::Path>& paths,
                                   const string& filename) {
  EPSImage eps;
  eps.Open(filename.c_str());

  double minX = 1E30, maxX = -1E30;
  double minY = 1E30, maxY = -1E30;

  // Bounding Box
  if (FLAGS_crop) {
    for (const auto &path : paths) {
      for (SizeType i = 0; i < path.NumVertices(); ++i) {
        const Point &p = ps_[path.VertexByOrder(i)];
        minX = min(minX, p.real());
        maxX = max(maxX, p.real());
        minY = min(minY, p.imag());
        maxY = max(maxY, p.imag());
      }
    }
  } else {
    for (VertexType v = 0; v < g_.NumVertices(); ++v) {
      const Point &p = ps_[v];
      minX = min(minX, p.real());
      maxX = max(maxX, p.real());
      minY = min(minY, p.imag());
      maxY = max(maxY, p.imag());
    }
  }

  // Margin
  {
    double w = maxX - minX;
    minX -= FLAGS_margin * w;
    maxX += FLAGS_margin * w;
  }
  {
    double h = maxY - minY;
    minY -= FLAGS_margin * h;
    maxY += FLAGS_margin * h;
  }

  // Aspect ratio
  if (FLAGS_aspect_ratio > 0) {
    double w = max(maxX - minX, (maxY - minY) / FLAGS_aspect_ratio);
    {
      double x = (maxX + minX) / 2;
      minX = x - w / 2;
      maxX = x + w / 2;
    }
    {
      double y = (maxY + minY) / 2;
      double h = w * FLAGS_aspect_ratio;
      minY = y - h / 2;
      maxY = y + h / 2;
    }
  }

  eps.SetBoundingBox(minX, maxX, minY, maxY);

  // Background
  if (FLAGS_draw_background) {
    eps.SetColor(0.6, 0.6, 0.6);
    eps.SetLineWidth(0.5);
    for (VertexType u = 0; u < g_.NumVertices(); ++u) {
      for (SizeType i = 0; i < g_.Degree(u); ++i) {
        const Edge &e = g_.EdgeFrom(u, i);
        VertexType v = e.To();
        eps.MoveTo(ps_[u]);
        eps.LineTo(ps_[v]);
        eps.Stroke();
      }
    }
  }

  // Representative paths]
  auto colors = GenerateColors(paths.size());
  eps.SetLineWidth(5.0);
  for (SizeType i = paths.size(); i-- > 0; ) {
    const Path &path = paths[i];
    PrintWeightVector(path.Cost());
    eps.SetColor(colors[i]);

    eps.MoveTo(ps_[path.From()]);
    for (SizeType i = 1; i < path.NumVertices(); ++i) {
      eps.LineTo(ps_[path.VertexByOrder(i)]);
    }
    eps.Stroke();
  }
}

vector<Color> PathVisualizer::GenerateColors(SizeType n) {
  vector<Color> cs(n);
  for (SizeType i = 0; i < n; ++i) {
    cs[i] = Color::FromHSV(0 + i / ((double)n - 1) * 4 / 6.0, 1.0, 0.9);
  }
  return cs;
}
}  // namespace utility
