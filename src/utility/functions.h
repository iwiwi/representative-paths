#pragma once

#include <string>
#include <vector>
#include <sstream>

namespace utility {
template<typename T>
std::vector<T> ParseSpaceSeparatedString(const std::string &str) {
  std::vector<T> res;
  std::istringstream ss(str);
  for (T t; ss >> t; ) res.emplace_back(t);
  return res;
}

template<typename T>
std::vector<T> ParseCommaSeparatedString(std::string str) {
  replace(str.begin(), str.end(), ',', ' ');
  return ParseSpaceSeparatedString<T>(str);
}
}  // namespace utility
