#pragma once
#include "common/common.h"
#include "utility/eps_image.h"
#include "utility/graph_coordinate.h"

namespace utility {
class PathVisualizer {
 public:
  PathVisualizer(const multi_cost_graph::Graph &g, const GraphCoordinate &c);

  void OutputAll(const vector<multi_cost_graph::Path> &paths, const string &filename_suffix);

  void OutputCostTSV(const vector<multi_cost_graph::Path> &paths, const string &filename);
  void OutputPathEPS(const vector<multi_cost_graph::Path> &paths, const string &filename);

 private:
  using Point = EPSImage::Point;

  const multi_cost_graph::Graph &g_;
  vector<Point> ps_;

  vector<Color> GenerateColors(SizeType n);
};
}  // namespace utility
