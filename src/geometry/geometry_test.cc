#include "geometry.h"
#include "gtest/gtest.h"
#include "common/common.h"
using namespace geometry;

namespace {
void TestSolveLinearSystem(WeightMatrix A, WeightVector b, WeightVector *res) {
  SizeType n = A.size();
  for (SizeType i = 0; i < n; ++i) {
    ASSERT_EQ(A[i].size(), n);
  }
  ASSERT_EQ(b.size(), n);

  WeightMatrix A_original(A);

  WeightVector x;
  ASSERT_TRUE(SolveLinearSystem(A, b, &x));

  for (SizeType i = 0; i < n; ++i) {
    ASSERT_TRUE(IsEqual(Dot(A_original[i], x), b[i]));
  }

  for (SizeType i = 0; i < n; ++i) {
    for (SizeType j = 0; j < n; ++j) {
      ASSERT_TRUE(IsEqual(A[i][j], i == j ? 1.0 : 0.0));
    }
  }

  if (res != nullptr) res->swap(x);
}

void TestPlaneFromPoints(const vector<WeightVector> &points) {
  Plane plane;
  ASSERT_TRUE(PlaneFromPoints(points, &plane));

  for (const auto &p : points) {
    ASSERT_TRUE(plane.DoesContain(p));
  }

  for (const auto &p : points) {
    if (InfinityDimension(p) != kInfinitySize) continue;
    for (const auto &q : points) {
      if (InfinityDimension(q) != kInfinitySize) continue;
      if (p == q) continue;

      WeightVector v = Sub(q, p);
      ASSERT_TRUE(IsZero(Dot(v, plane.normal_vector)));
    }
  }
}
}  // namespace

TEST(SolveLinearSystem, trivial) {
  WeightMatrix A = {
      {1, 0, 0},
      {0, 1, 0},
      {0, 0, 1},
  };
  WeightVector b = {20, 300, 4000}, x;

  ASSERT_NO_FATAL_FAILURE(TestSolveLinearSystem(A, b, &x));

  for (int i = 0; i < 3; ++i) {
    ASSERT_TRUE(IsEqual(b[i], x[i]));
  }
}

TEST(SolveLinearSystem, random) {
  static const SizeType kNumTrials = 100;
  static const SizeType kSize = 3;

  for (SizeType trial = 0; trial < kNumTrials; ++trial) {
    WeightVector x = RandomWeightVectorUnitLength(kSize), b(kSize);
    WeightMatrix A(kSize);
    for (SizeType i = 0; i < kSize; ++i) {
      A[i] = RandomWeightVectorUnitLength(kSize);
      b[i] = Dot(A[i], x);
    }
    ASSERT_NO_FATAL_FAILURE(TestSolveLinearSystem(A, b, nullptr));
  }
}

TEST(PlaneFromPoints, trivial_2d) {
  vector<WeightVector> points = {
      {1, 0},
      {0, 1},
  };
  Plane plane;
  ASSERT_TRUE(PlaneFromPoints(points, &plane));

  ASSERT_TRUE(IsEqual(plane.normal_vector[0], sqrt(0.5)));
  ASSERT_TRUE(IsEqual(plane.normal_vector[1], sqrt(0.5)));
}

TEST(PlaneFromPoints, trivial_3d) {
  vector<WeightVector> points = {
      {0, 0, 1},
      {0, 1, 0},
      {1, 0, 0},
  };
  Plane plane;
  ASSERT_TRUE(PlaneFromPoints(points, &plane));

  ASSERT_TRUE(IsEqual(plane.normal_vector[0], sqrt(1.0 / 3.0)));
  ASSERT_TRUE(IsEqual(plane.normal_vector[1], sqrt(1.0 / 3.0)));
  ASSERT_TRUE(IsEqual(plane.normal_vector[2], sqrt(1.0 / 3.0)));
}

TEST(PlaneFromPoints, infinity_2d) {
  {
    vector<WeightVector> points = {
        {0, kInfinityWeight},
        {1, 0},
    };
    Plane plane;
    ASSERT_TRUE(PlaneFromPoints(points, &plane));

    ASSERT_TRUE(IsEqual(plane.normal_vector[0], 1.0));
    ASSERT_TRUE(IsEqual(plane.normal_vector[1], 0.0));
  }
  {
    vector<WeightVector> points = {
        {kInfinityWeight, 0},
        {0, 1},
    };
    Plane plane;
    ASSERT_TRUE(PlaneFromPoints(points, &plane));

    ASSERT_TRUE(IsEqual(plane.normal_vector[0], 0.0));
    ASSERT_TRUE(IsEqual(plane.normal_vector[1], 1.0));
  }
}

TEST(PlaneFromPoints, random_without_infinity) {
  static const SizeType kNumDimensions = 10;
  static const SizeType kNumTrials = 100;

  for (SizeType trial = 0; trial < kNumTrials; ++trial) {
    vector<WeightVector> points;
    for (SizeType d = 0; d < kNumDimensions; ++d) {
      points.emplace_back(RandomWeightVectorUnitLength(kNumDimensions));
    }

    ASSERT_NO_FATAL_FAILURE(TestPlaneFromPoints(points));
  }
}

TEST(PlaneFromPoints, random_with_infinity) {
  static const SizeType kDimension = 10;
  static const SizeType kNumTrials = 100;

  for (SizeType trial = 0; trial < kNumTrials; ++trial) {
    SizeType num_points_at_infinity = rand() % kDimension;
    vector<SizeType> dimensions(kDimension);
    iota(all(dimensions), 0);
    random_shuffle(all(dimensions));

    vector<WeightVector> points;
    for (SizeType d = 0; d < kDimension; ++d) {
      if (d < num_points_at_infinity) {
        points.emplace_back(kDimension, 0);
        points.back()[dimensions[d]] = kInfinityWeight;
      } else {
        points.emplace_back(RandomWeightVectorUnitLength(kDimension));
      }
    }
    random_shuffle(all(points));

    ASSERT_NO_FATAL_FAILURE(TestPlaneFromPoints(points));
  }
}

TEST(LowerEnvelope, small1) {
  vector<Plane> le = LowerEnvelope({{2, 2}});
  ASSERT_EQ(le.size(), SizeType(2));
  ASSERT_TRUE(le[0].IsEqualTo(Plane({0, 1}, 2)));
  ASSERT_TRUE(le[1].IsEqualTo(Plane({1, 0}, 2)));
}

TEST(LowerEnvelope, small2) {
  vector<Plane> le = LowerEnvelope({{2, 4}, {4, 2}});
  ASSERT_EQ(le.size(), SizeType(3));
  ASSERT_TRUE(le[0].IsEqualTo(Plane({0, 1}, 2)));
  ASSERT_TRUE(le[1].IsEqualTo(Plane({sqrt(0.5), sqrt(0.5)}, sqrt(0.5) * 6)));
  ASSERT_TRUE(le[2].IsEqualTo(Plane({1, 0}, 2)));
}

TEST(LowerEnvelope, small3) {
  vector<Plane> le = LowerEnvelope({
    {2, 2, 4},
    {2, 4, 2},
    {4, 2, 2},
  });
  ASSERT_EQ(le.size(), SizeType(4));

  const WeightType sqrt3 = sqrt(1.0 / 3.0);
  ASSERT_TRUE(le[0].IsEqualTo(Plane({0, 0, 1}, 2)));
  ASSERT_TRUE(le[1].IsEqualTo(Plane({0, 1, 0}, 2)));
  ASSERT_TRUE(le[2].IsEqualTo(Plane({sqrt3, sqrt3, sqrt3}, sqrt3 * 8)));
  ASSERT_TRUE(le[3].IsEqualTo(Plane({1, 0, 0}, 2)));
}

TEST(LowerEnvelope, random_easy) {
  static const SizeType kNumTrial = 100;
  static const SizeType kNumDimensions = 5;
  static const SizeType kNumPoints = 7;

  cout << "Numbers of planes:";
  for (SizeType trial = 0; trial < kNumTrial; ++trial) {
    vector<WeightVector> points;
    for (SizeType i = 0; i < kNumPoints; ++i) {
      points.emplace_back(RandomWeightVectorRandomLength(kNumDimensions));
    }
    vector<Plane> planes = LowerEnvelope(points);
    cout << " " << planes.size();

    for (const auto &plane : planes) {
      for (const auto &point : points) {
        ASSERT_GE(plane.Position(point), 0);
      }
    }
  }
  cout << endl;
}

TEST(LowerEnvelope, random_thorough) {
  static const SizeType kNumTrial = 30;
  static const SizeType kNumDimensions = 5;
  static const SizeType kNumDataPoints = 7;
  static const SizeType kNumTestPoints = 30;
  static const SizeType kNumWitnessVectors = 10000;

  for (SizeType trial = 0; trial < kNumTrial; ++trial) {
    vector<WeightVector> data_points;
    for (SizeType i = 0; i < kNumDataPoints; ++i) {
      data_points.emplace_back(RandomWeightVectorRandomLength(kNumDimensions));
    }
    vector<Plane> planes = LowerEnvelope(data_points);

    SizeType num_tests_in = 0, num_tests_out = 0;
    for (SizeType test = 0; test < kNumTestPoints; ++test) {
      const WeightVector p = Mul(RandomWeightVectorRandomLength(kNumDimensions), 1.0);

      bool in_planes = true;
      for (const auto &plane : planes) {
        if (!(in_planes &= plane.Position(p) >= 0)) break;
      }
      ++(in_planes ? num_tests_in : num_tests_out);

      bool in_witness = true;
      for (SizeType witness = 0; witness < kNumWitnessVectors; ++witness) {
        const WeightVector v = witness < planes.size() ?
            planes[witness].normal_vector : RandomWeightVectorUnitLength(kNumDimensions);

        WeightType min_dot = kInfinityWeight;
        for (const auto &point : data_points) min_dot = min(min_dot, Dot(v, point));

        if (!(in_witness &= IsLessThan(min_dot, Dot(v, p)))) break;
      }

      ASSERT_EQ(in_planes, in_witness);
    }

    cout << "Planes: " << planes.size()
        << ", Inside: " << num_tests_in
        << ", Outside: " << num_tests_out << endl;
  }
  cout << endl;
}

TEST(AddPointToLowerEnvelope, random) {
  static const SizeType kNumTrial = 100;
  static const SizeType kNumDimensions = 5;
  static const SizeType kNumPoints = 7;

  for (SizeType trial = 0; trial < kNumTrial; ++trial) {
    vector<WeightVector> points;
    vector<Plane> planes;

    for (SizeType i = 0; i < kNumPoints; ++i) {
      WeightVector new_point(RandomWeightVectorRandomLength(kNumDimensions));
      vector<Plane> new_planes(AddPointToLowerEnvelope(points, new_point, planes));
      FilterLowerEnvelope(new_point, &planes);

      // TODO: investigate why |inplace_merge| does not work
      planes.insert(planes.end(), new_planes.begin(), new_planes.end());
      sort(all(planes));
      //inplace_merge(planes.begin(), planes.end() - new_planes.size(), planes.end());

      points.emplace_back(new_point);
      vector<Plane> planes2 = LowerEnvelope(points);

      ASSERT_EQ(planes.size(), planes2.size());
      for (SizeType i = 0; i < planes.size(); ++i) {
        ASSERT_TRUE(planes[i].IsEqualTo(planes2[i]));
      }
    }
  }
}
