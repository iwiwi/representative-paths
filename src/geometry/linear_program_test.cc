#include "linear_program.h"
#include "gtest/gtest.h"
#include "common/common.h"
using namespace geometry;

TEST(LinearProgram, trivial1) {
  LinearProgram lp;
  ASSERT_TRUE(IsZero(lp.Solve({1, 1, 1}, nullptr)));
}

TEST(LinearProgram, trivial2) {
  LinearProgram lp;
  lp.AddConstraint({1, 0}, 1.0);
  ASSERT_TRUE(IsZero(lp.Solve({0, 1}, nullptr)));
}

TEST(LinearProgram, trivial3) {
  LinearProgram lp;
  lp.AddConstraint({1, 0}, 1.0);
  lp.AddConstraint({0, 1}, 1.0);
  ASSERT_TRUE(IsEqual(1.0, lp.Solve({0, 1}, nullptr)));
}

TEST(LinearProgram, random) {
  constexpr SizeType kMaxD = 20;
  constexpr SizeType kMaxN = 20;
  constexpr SizeType kNumProblems = 100;
  constexpr SizeType kNumTrial = 3;
  constexpr SizeType kNumRandomPoints = 1000;

  for (SizeType problem = 0; problem < kNumProblems; ++problem) {
    const SizeType d = 1 + rand() % kMaxD;
    const SizeType n = 1 + rand() % kMaxN;

    // Create a problem
    vector<pair<WeightVector, WeightType>> cs(n);
    LinearProgram lp;
    for (SizeType i = 0; i < n; ++i) {
      cs[i].first = RandomWeightVectorUnitLength(d);
      cs[i].second = rand() / (double)RAND_MAX;
      lp.AddConstraint(cs[i].first, cs[i].second);
    }

    // Solve |kNumTrial| times
    for (SizeType trial = 0; trial < kNumTrial; ++trial) {
      const WeightVector w = RandomWeightVectorUnitLength(d);
      WeightVector x;
      const WeightType r = lp.Solve(w, &x);

      ASSERT_TRUE(IsEqual(r, Dot(w, x)));

      // Verify the optimality by random vectors
      for (SizeType point = 0; point < kNumRandomPoints; ++point) {
        WeightVector y = RandomWeightVectorUnitLength(d);
        for (SizeType i = 0; i < n; ++i) {
          const auto &c = cs[i];
          WeightType d = Dot(c.first, y);
          if (d < c.second) y = Mul(y, c.second / d);
        }

        // Check |y| is a feasible solution
        for (SizeType i = 0; i < n; ++i) {
          const auto &c = cs[i];
          ASSERT_TRUE(!IsLessThan(Dot(c.first, y), c.second));
        }

        // Check |y| is not better than |x|
        ASSERT_TRUE(!IsLessThan(Dot(w, y), Dot(w, x)));
      }
    }
  }
}
