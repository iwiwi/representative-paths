#include "common/common.h"
#include "linear_program.h"

namespace geometry {
namespace {
typedef vector<WeightType> vec_t;
typedef vector<vec_t> mat_t;
constexpr WeightType EPS = kEpsWeight;

// maximize c^T x
// s.t. Ax <= b, x >= 0
pair<double, vec_t> crisscross(const mat_t &A, const vec_t &b, const vec_t &c) {
  constexpr SizeType kMaxIterations = 1000;

  int m = A.size(), n = c.size();
  mat_t D(m + 1, vec_t(n + 1));
  vector<int> id(n + m + 1);
  for (int i = 0; i <= n + m; i++) id[i] = i;
  for (int i = 0; i < m; i++) {
    for (int j = 0; j < n; j++) D[i][j] = -A[i][j];
    D[i][n] = b[i];
  }
  for (int j = 0; j < n; j++) D[m][j] = c[j];
  for (SizeType i = 0; ; ++i) {
    if (i >= kMaxIterations) {
      cout << "Criscross: Too many iterations." << endl;
      return make_pair(0, vec_t());
    }

    int r = m, s = n + m;
    for (int i = 0; i < m; i++) if (D[i][n] < -EPS && id[n + r] > id[n + i]) r = i;
    for (int j = 0; j < n; j++) if (D[m][j] > EPS && id[s] > id[j]) s = j;
    if (r == m && s == n + m) break;
    if (id[n + r] < id[s]) {
      s = n + m;
      for (int j = 0; j < n; j++) if (D[r][j] > EPS && id[s] > id[j]) s = j;
    } else {
      r = m;
      for (int i = 0; i < m; i++) if (D[i][s] < -EPS && id[n + r] > id[n + i]) r = i;
    }
    if (r == m) return make_pair(1E30, vec_t());       // unbounded
    if (s == n + m) return make_pair(-1E30, vec_t());  // unfeasible
    int t = id[s]; id[s] = id[n + r]; id[n + r] = t;
    D[r][s] = 1.0 / D[r][s];
    for (int j = 0; j <= n; j++) if (j != s) D[r][j] *= -D[r][s];
    for (int i = 0; i <= m; i++) if (i != r && abs(D[i][s]) > EPS) {
        for (int j = 0; j <= n; j++) if (j != s) D[i][j] += D[r][j] * D[i][s];
        D[i][s] *= D[r][s];
      }
  }
  vec_t x(n);
  for (int i = 0; i < m; i++) if (id[n + i] < n) x[id[n + i]] = D[i][n];
  return make_pair(D[m][n], x);
}
}  // namespace

WeightType LinearProgram::Solve(const WeightVector &w, WeightVector *x) {
  SizeType d = w.size(), n = constraints_.size();

  mat_t A(n);
  vec_t b(n);
  for (SizeType i = 0; i < n; ++i) {
    const auto &c = constraints_[i];
    assert(c.first.size() == d);
    A[i] = Mul(c.first, -1);
    b[i] = -c.second;
  }

  auto r = crisscross(A, b, Mul(w, -1));
  // PrintWeightVector(r.second);

  // TODO: remove me
  {
    WeightVector x = r.second;
    if (x.empty()) goto done;
    for (SizeType i = 0; i < d; ++i) {
      assert(!IsLessThan(x[i], 0.0));
    }
    for (SizeType i = 0; i < n; ++i) {
      const auto &c = constraints_[i];
      // assert(!IsLessThan(Dot(c.first, x), c.second));
      if (IsLessThan(Dot(c.first, x), c.second)) {
        cout << "Crisscross: BUG!!" << endl;  // TODO: fix me
        return 0.0;
      }
    }
    done:;
  }

  if (x != nullptr) *x = r.second;
  return -r.first;
}
}  // namespace geometry
