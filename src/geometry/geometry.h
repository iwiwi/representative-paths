#pragma once
#include "common/common.h"

namespace geometry {
// Represents a plane "normal_vector * X + offset"
struct Plane {
  WeightVector normal_vector;
  WeightType offset;

  Plane() : normal_vector(), offset() {}
  Plane(const WeightVector &nv, WeightType of) : normal_vector(nv), offset(of) {}

  int Position(const WeightVector &point) const {
    WeightType d = Dot(point, normal_vector);
    return IsEqual(d, offset) ? 0 : (d > offset ? +1 : -1);
  }

  bool DoesContain(const WeightVector &point) const;

  void Print(ostream &os = cerr) const {
    PrintWeightVector(normal_vector, os);
    os << " + " << offset << endl;
  }

  bool IsEqualTo(const Plane &p) const {
    return IsEqual(normal_vector, p.normal_vector) && IsEqual(offset, p.offset);
  }
};

inline bool operator < (const Plane &a, const Plane& b) {
  return tie(a.normal_vector, a.offset) < tie(b.normal_vector, b.offset);
}

// Linear System
typedef vector<WeightVector> WeightMatrix;

inline void PrintWeightMatrix(const WeightMatrix &a, ostream &os = cerr) {
  for (SizeType i = 0; i < a.size(); ++i) {
    PrintWeightVector(a[i], os);
    os << std::endl;
  }
}

inline WeightVector PointAtInfinity(SizeType num_dimensions, SizeType infinity_dimension) {
  WeightVector p(num_dimensions);
  p[infinity_dimension] = kInfinityWeight;
  return p;  // move
}

inline SizeType InfinityDimension(const WeightVector &point) {
  for (SizeType d = 0; d < point.size(); ++d) {
    if (point[d] == kInfinityWeight) return d;
  }
  return kInfinitySize;
}

bool SolveLinearSystem(WeightMatrix &A, const WeightVector &b, WeightVector *x);

bool PlaneFromPoints(const vector<WeightVector> &points, Plane *plane);

bool IsPlaneOfLowerEnvelope(const Plane &plane, const vector<WeightVector> &points);

vector<Plane> LowerEnvelope(const vector<WeightVector> &points);

// Returns new planes
vector<Plane> AddPointToLowerEnvelope(const vector<WeightVector> &previous_points,
                                      const WeightVector &new_point,
                                      const vector<Plane> &previous_planes);

void FilterLowerEnvelope(const WeightVector &new_point, vector<Plane> *previous_planes);
}  // namespace geometry
