#pragma once
#include "common/common.h"

namespace geometry {
class LinearProgram {
 public:
  /*
    Minimize: w * x
    Subject to:
      (1) wi * x >= ci
      (2) x >= 0

    * Vectors w and wi should be non-negative.
    * Therefore, w * x >= 0
  */

  void AddConstraint(const WeightVector &wi, WeightType ci) {
    constraints_.emplace_back(wi, ci);
  }

  WeightType Solve(const WeightVector &w, WeightVector *x);

 private:
  vector<pair<WeightVector, WeightType>> constraints_;
};
}  // namespace geometry
