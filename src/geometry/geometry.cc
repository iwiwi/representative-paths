#include "geometry.h"
#include "common/common.h"

namespace geometry {
bool Plane::DoesContain(const WeightVector &point) const {
  SizeType infd = InfinityDimension(point);
  if (infd != kInfinitySize) return IsZero(normal_vector[infd]);
  return Position(point) == 0;
}

bool SolveLinearSystem(WeightMatrix &A, const WeightVector &b, WeightVector *x) {
  SizeType n = A.size();
  assert(n > 0);
  assert(A[0].size() == n);
  for (SizeType i = 0; i < n; ++i) A[i].resize(n + 1, b[i]);

  for (SizeType k = 0; k < n; ++k) {
    // Pivot selection
    for (SizeType i = k + 1; i < n; ++i) {
      if (Abs(A[k][k]) < Abs(A[i][k])) A[k].swap(A[i]);
    }
    if (IsZero(A[k][k])) return false;

    // Normalize
    {
      WeightType p = A[k][k];
      for (SizeType j = k; j <= n; ++j) A[k][j] /= p;
    }

    // Eliminate
    for (SizeType i = 0; i < n; ++i) {
      if (i == k) continue;
      WeightType q = A[i][k];
      for (SizeType j = k; j <= n; ++j) A[i][j] -= q * A[k][j];
    }
  }

  assert(x != nullptr);
  x->resize(n);
  for (SizeType i = 0; i < n; ++i) {
    x->at(i) = A[i].back();
    A[i].pop_back();
  }
  return true;
}

bool PlaneFromPoints(const vector<WeightVector> &points, Plane *plane) {
  const SizeType D = points[0].size();
  assert(points.size() == D);

  // Find points at infinity
  vector<bool> is_point_infinity(D);
  vector<bool> is_dimension_arbitrary(D);
  SizeType base_p = kInfinitySize;

  for (SizeType p = 0; p < D; ++p) {
    for (SizeType d = 0; d < D; ++d) {
      if (points[p][d] == kInfinityWeight) {
        is_point_infinity[p] = true;
        is_dimension_arbitrary[d] = true;
      }
    }
    if (base_p == kInfinitySize && !is_point_infinity[p]) {
      base_p = p;
    }
  }

  // There should be at least one data point
  if (base_p == kInfinitySize) return false;

  // Shrink the dimensions
  vector<SizeType> d_to_sd(D, kInfinitySize), sd_to_d;
  for (SizeType d = 0; d < D; ++d) {
    if (is_dimension_arbitrary[d]) continue;
    d_to_sd[d] = sd_to_d.size();
    sd_to_d.emplace_back(d);
  }
  const SizeType SD = sd_to_d.size();
  assert(SD > 0);

  // Construct a linear system
  WeightMatrix A;
  for (SizeType p = base_p + 1; p < D; ++p) {
    if (is_point_infinity[p]) continue;

    WeightVector a(SD);
    for (SizeType sd = 0; sd < SD; ++sd) {
      SizeType d = sd_to_d[sd];
      a[sd] = points[p][d] - points[base_p][d];
    }
    A.emplace_back(move(a));
  }
  A.emplace_back(SD, 1.0);  // Unit length
  assert(A.size() == SD);
  WeightVector b(SD);
  b.back() = 1.0;

  // Solve the linear system
  WeightVector x;
  if (!SolveLinearSystem(A, b, &x)) return false;
  if (SigNum(*min_element(all(x))) < 0) x = Mul(x, -1);
  x = Mul(x, 1.0 / Abs(x));

  // Unshrink and compute the offset
  assert(plane != nullptr);
  plane->normal_vector.resize(D);
  for (SizeType d = 0; d < D; ++d) {
    if (is_dimension_arbitrary[d]) plane->normal_vector[d] = 0.0;
    else plane->normal_vector[d] = x[d_to_sd[d]];
  }
  plane->offset = Dot(plane->normal_vector, points[base_p]);

  return true;  // Finally!
}

bool IsPlaneOfLowerEnvelope(const Plane &plane, const vector<WeightVector> &points) {
  // All vector element should be non-negative
  if (SigNum(*min_element(all(plane.normal_vector))) < 0) {
    assert(SigNum(*max_element(all(plane.normal_vector))) > 0);
    return false;
  }
  for (const auto &p : points) {
    if (plane.Position(p) < 0) return false;
  }
  return true;
}

void PurifyPlane(Plane &plane) {
  // In theory, elements in |plane.normal_vector| should be non-negative.
  // However, due to numerical errors, sometimes negative numbers occur,
  // which cause serious problems in shortest-path computation.
  for (auto &x : plane.normal_vector) x = max(x, 0.0);
  WeightType a = 1.0 / Abs(plane.normal_vector);
  for (auto &x : plane.normal_vector) x *= a;
}

vector<Plane> LowerEnvelope(const vector<WeightVector> &original_points) {
  assert(original_points.size() > 0);
  SizeType D = original_points[0].size();
  assert(D >= 2);

  vector<WeightVector> candidate_points(original_points);
  for (SizeType d = 0; d < D; ++d) candidate_points.emplace_back(PointAtInfinity(D, d));
  SizeType N = candidate_points.size();

  // Generate all the subsets of size D
  vector<Plane> planes;
  assert(N < 64);
  for (uint64_t m = (uint64_t(1) << D) - 1; m < (uint64_t(1) << N); ) {
    {
      vector<WeightVector> selected_points;
      for (SizeType i = 0; i < N; ++i) {
        if (m & (uint64_t(1) << i)) selected_points.emplace_back(candidate_points[i]);
      }
      assert(selected_points.size() == D);

      Plane plane;
      if (!PlaneFromPoints(selected_points, &plane)) goto ng;
      if (!IsPlaneOfLowerEnvelope(plane, original_points)) goto ng;
      PurifyPlane(plane);
      planes.emplace_back(move(plane));
    }

    ng:
    {
      uint64_t x = m & -m, y = m + x;
      m = ((m & ~y) / x >> 1) | y;
    }
  }

  sort(all(planes));
  auto f = [](const Plane &a, const Plane &b){
    return a.IsEqualTo(b);
  };
  planes.erase(unique(all(planes), f), planes.end());
  return planes;
}

// TODO: bad function name
vector<Plane> AddPointToLowerEnvelope(const vector<WeightVector> &previous_points,
                                      const WeightVector &new_point,
                                      const vector<Plane> &previous_planes) {
  SizeType D = new_point.size();
  assert(D >= 2);

  vector<WeightVector> candidate_points(previous_points);
  for (SizeType d = 0; d < D; ++d) candidate_points.emplace_back(PointAtInfinity(D, d));
  SizeType N = candidate_points.size();

  // Generate all the subsets of size D - 1
  vector<Plane> new_planes;
  assert(N < 64);
  for (uint64_t m = (uint64_t(1) << (D - 1)) - 1; m < (uint64_t(1) << N); ) {
    {
      vector<WeightVector> selected_points;
      for (SizeType i = 0; i < N; ++i) {
        if (m & (uint64_t(1) << i)) selected_points.emplace_back(candidate_points[i]);
      }
      selected_points.emplace_back(new_point);
      assert(selected_points.size() == D);

      Plane plane;
      if (!PlaneFromPoints(selected_points, &plane)) goto ng;
      for (const auto &previous_plane : previous_planes) {
        if (previous_plane.IsEqualTo(plane)) goto ng;
      }
      if (!IsPlaneOfLowerEnvelope(plane, previous_points)) goto ng;
      PurifyPlane(plane);
      new_planes.emplace_back(move(plane));
    }

    ng:
    {
      uint64_t x = m & -m, y = m + x;
      m = ((m & ~y) / x >> 1) | y;
    }
  }

  return new_planes;
}

void FilterLowerEnvelope(const WeightVector &new_point, vector<Plane> *previous_planes) {
  auto f = [&](const Plane &plane) {
    return plane.Position(new_point) < 0;
  };
  auto i = remove_if(previous_planes->begin(), previous_planes->end(), f);
  previous_planes->erase(i, previous_planes->end());
}
};  // namespace geometry
