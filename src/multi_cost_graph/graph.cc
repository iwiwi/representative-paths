#include "graph.h"
#include "multi_cost_graph.h"
#include "common/common.h"

namespace multi_cost_graph {
namespace {
template<typename T>
void ReadValue(istream &is, T *t) {
  CHECK(is.read((char*)t, sizeof(T)));
}

template<typename T>
void WriteValue(ostream &is, const T &t) {
  is.write((char*)&t, sizeof(T));
}
}  // namespace

void Graph::Construct(vector<tuple<VertexType, VertexType, WeightVector>> edge_tuples) {
  typedef tuple<VertexType, VertexType, WeightVector> EdgeTupleType;
  JLOG_PUT("graph.num_original_edges", edge_tuples.size());

  num_dimensions_ = edge_tuples.empty() ? 0 : get<2>(edge_tuples[0]).size();
  num_vertices_ = 0;
  for (const auto &t : edge_tuples) {
    num_vertices_ = max(num_vertices_, max(get<0>(t), get<1>(t)) + 1);
  }

  sort(all(edge_tuples));
  edge_tuples.erase(unique(all(edge_tuples)), edge_tuples.end());
  {
    auto f = [](const EdgeTupleType &a, const EdgeTupleType &b) {
      const auto &ap = make_pair(min(get<0>(a), get<1>(a)), max(get<0>(a), get<1>(a)));
      const auto &bp = make_pair(min(get<0>(b), get<1>(b)), max(get<0>(b), get<1>(b)));
      return tie(get<2>(a), ap) < tie(get<2>(b), bp);
    };
    sort(all(edge_tuples), f);
  }
  JLOG_PUT("graph.num_uniqued_edges", edge_tuples.size());

  SizeType num_representing_edges_ = 0;
  {
    vector<SizeType> deg(num_vertices_, 0);
    for (SizeType i = 0; i < edge_tuples.size(); ++i) {
      const auto &t = edge_tuples[i];
      if (i > 0) {
        const auto &p = edge_tuples[i - 1];
        if (tie(get<1>(p), get<0>(p), get<2>(p)) == t) continue;
      }
      ++deg[get<0>(t)];
      ++deg[get<1>(t)];
      num_representing_edges_ += 2;
    }

    index_.resize(num_vertices_ + 1);
    index_[0] = 0;
    for (VertexType v = 0; v < num_vertices_; ++v) {
      index_[v + 1] = index_[v] + deg[v];
    }
    assert(index_.back() == num_representing_edges_);
  }

  edge_area_.reset(new uint8_t[EdgeSize() * num_representing_edges_]);
  {
    vector<SizeType> done(num_vertices_, 0);
    for (SizeType i = 0; i < edge_tuples.size(); ++i) {
      const auto &t = edge_tuples[i];
      bool is_bidirectional = false;
      if (i + 1 < edge_tuples.size()) {
        const auto &p = edge_tuples[i + 1];
        is_bidirectional = (tie(get<1>(p), get<0>(p), get<2>(p)) == t);
      }

      VertexType from = get<0>(t);
      VertexType to = get<1>(t);

      // Forward
      assert(index_[from] + done[from] < index_[from + 1]);
      Edge &ef = EdgeByID(index_[from] + done[from]++);
      ef.to_ = to;
      ef.has_fwd_ = true;
      ef.has_bwd_ = is_bidirectional;

      // Backward
      assert(index_[to] + done[to] < index_[to + 1]);
      Edge &eb = EdgeByID(index_[to] + done[to]++);
      eb.to_ = from;
      eb.has_fwd_ = is_bidirectional;
      eb.has_bwd_ = true;

      assert(get<2>(t).size() == num_dimensions_);
      for (SizeType d = 0; d < num_dimensions_; ++d) {
        ef.cost_[d] = eb.cost_[d] = get<2>(t)[d];
      }

      if (is_bidirectional) ++i;
    }

    for (VertexType v = 0; v < num_vertices_; ++v) {
      assert(index_[v] + done[v] == index_[v + 1]);
    }
  }

  JLOG_OPEN("graph") {
    JLOG_PUT("num_vertices", num_vertices_);
    JLOG_PUT("num_dimensions", num_dimensions_);
    JLOG_PUT("num_edges", NumEdges());
    JLOG_PUT("num_representing_edges", NumRepresentingEdges());
  }
}

void Graph::Read(const char *file_path) {
  Graph::ReadInternal(file_path, false, 0);
}

void Graph::Read(istream &is) {
  Graph::ReadInternal(is, false, 0);
}

void Graph::ReadWithSpecifiedDimension(const char *file_path, SizeType num_dimensions) {
  Graph::ReadInternal(file_path, true, num_dimensions);
}

void Graph::ReadWithSpecifiedDimension(istream &is, SizeType num_dimensions) {
  Graph::ReadInternal(is, true, num_dimensions);
}

void Graph::ReadInternal(const char *file_path,
                         bool is_dimension_specified,
                         SizeType num_specified_dimensions) {
  JLOG_PUT("graph.file", file_path);
  ifstream ifs(file_path, ios::binary);
  CHECK(ifs);
  ReadInternal(ifs, is_dimension_specified, num_specified_dimensions);
}

void Graph::ReadInternal(istream &is,
                         bool is_dimension_specified,
                         SizeType num_specified_dimensions) {
  SizeType num_representing_edges, num_original_dimensions;
  ReadValue<VertexType>(is, &num_vertices_);
  ReadValue<SizeType>(is, &num_representing_edges);
  ReadValue<SizeType>(is, &num_original_dimensions);
  num_dimensions_ = is_dimension_specified ? num_specified_dimensions : num_original_dimensions;

  index_.resize(num_vertices_ + 1);
  index_.back() = num_representing_edges;
  edge_area_.reset(new uint8_t[EdgeSize() * num_representing_edges]);

  vector<common::Random> rnd(num_specified_dimensions);
  for (SizeType d = 0; d < num_specified_dimensions; ++d) rnd[d] = FLAGS_random_seed * d;

  SizeType num_read_edges = 0;
  for (VertexType v = 0; v < num_vertices_; ++v) {
    index_[v] = num_read_edges;

    SizeType deg;
    ReadValue<SizeType>(is, &deg);
    for (SizeType i = 0; i < deg; ++i) {
      Edge &e = EdgeByID(num_read_edges++);

      ReadValue<VertexType>(is, &e.to_fwd_bwd_);

      SizeType d = 0;
      for (; d < min(num_original_dimensions, num_dimensions_); ++d) {
        ReadValue<WeightStorageType>(is, &e.cost_[d]);
        assert(e.cost_[d] >= 0);
      }
      for (; d < max(num_original_dimensions, num_dimensions_); ++d) {
        if (d < num_original_dimensions) {
          WeightStorageType w;
          ReadValue<WeightStorageType>(is, &w);
        }
        if (d < num_dimensions_) {
          e.cost_[d] = rnd[d].Next() / (double)rnd[d].Max();
        }
      }
    }
  }
  CHECK(is.peek() == std::ios::traits_type::eof());
  CHECK(num_read_edges == num_representing_edges);

  JLOG_OPEN("graph") {
    JLOG_PUT("num_vertices", num_vertices_);
    JLOG_PUT("num_edges", NumEdges());
    JLOG_PUT("num_representing_edges", NumRepresentingEdges());
    JLOG_PUT("num_original_dimensions", num_original_dimensions);
    JLOG_PUT("num_dimensions", NumDimensions());
  }
}

void Graph::PrintSummary(ostream &os) const {
  os
    << "Vertices:\t" << num_vertices_ << endl
    << "Edges:\t" << NumEdges() << endl
    << "Dimensions:\t" << NumDimensions() << endl;
}

void Graph::Print(ostream &os) const {
  for (VertexType v = 0; v < num_vertices_; ++v) {
    os << v << "\t";
    for (SizeType i = 0; i < Degree(v); ++i) {
      auto &e = EdgeFrom(v, i);
      e.Print(os, NumDimensions());
      os << "\t";
    }
    os << endl;
  }
}

void Graph::PrintTSV(ostream &os) const {
  for (VertexType v = 0; v < num_vertices_; ++v) {
    for (SizeType i = 0; i < Degree(v); ++i) {
      auto &e = EdgeFrom(v, i);
      if (!e.HasForward()) continue;

      os << v << "\t" << e.To();
      for (SizeType d = 0; d < NumDimensions(); ++d) {
        os << "\t" << (double)e.Cost()[d];
      }
      os << endl;
    }
  }
}

void Graph::Write(const char *file_path) const {
  ofstream ofs(file_path, ios::binary);
  CHECK(ofs);
  Write(ofs);
}

void Graph::Write(ostream &os) const {
  CHECK(os);
  WriteValue<VertexType>(os, num_vertices_);
  WriteValue<SizeType>(os, NumRepresentingEdges());
  WriteValue<SizeType>(os, num_dimensions_);

  for (VertexType v = 0; v < num_vertices_; ++v) {
    SizeType deg = Degree(v);
    WriteValue<SizeType>(os, deg);

    for (SizeType i = 0; i < deg; ++i) {
      const Edge &e = EdgeFrom(v, i);
      WriteValue<VertexType>(os, e.to_fwd_bwd_);

      for (SizeType d = 0; d < NumDimensions(); ++d) {
        WriteValue<WeightStorageType>(os, e.Cost()[d]);
      }
    }
  }
}

SizeType Graph::NumEdges(DirectionType direction) const {
  SizeType n = 0;
  for (SizeType i = 0; i < NumRepresentingEdges(); ++i) {
    if (EdgeByID(i).HasDirection(direction)) ++n;
  }
  return n;
}

SizeType Graph::NumBidirectionalEdges() const {
  SizeType n = 0;
  for (SizeType i = 0; i < NumRepresentingEdges(); ++i) {
    if (EdgeByID(i).HasDirection(kDirectionForward) &&
        EdgeByID(i).HasDirection(kDirectionBackward)) ++n;
  }
  return n;
}

SizeType Graph::Hash() const {
  constexpr SizeType kHashBase = 31;
  SizeType h = 0;

  h = h * kHashBase + NumVertices();
  h = h * kHashBase + NumEdges();
  h = h * kHashBase + NumDimensions();

  for (const auto &i : index_) {
    h = h * kHashBase + i;
  }

  for (SizeType i = 0; i < NumRepresentingEdges(); ++i) {
    const auto &e = EdgeByID(i);
    h = h * kHashBase + e.To();
    h = h * kHashBase + e.HasForward();
    h = h * kHashBase + e.HasBackward();
    for (SizeType d = 0; d < NumDimensions(); ++d) {
      h = h * kHashBase + e.Cost()[d];
    }
  }

  return h;
}
}  // namespace multi_cost_graph
