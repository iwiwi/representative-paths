#include "dijkstra.h"
#include "common/binary_heap.h"
#include "common/common.h"

namespace multi_cost_graph {
vector<WeightType> Dijkstra(const Graph &graph, VertexType from,
                            const WeightVector &preference,
                            DirectionType direction) {
  VertexType num_vs = graph.NumVertices();

  vector<WeightType> pot(num_vs, kInfinityWeight);
  vector<pair<VertexType, const Edge*>> par(num_vs, make_pair(kDummyVertex, nullptr));

  BinaryHeap que(graph.NumVertices());
  que.Reset();

  pot[from] = 0;
  que.Update(from, 0);
  assert(!que.IsEmpty());
  while (!que.IsEmpty()) {
    VertexType v;
    WeightType d;
    tie(v, d) = que.Pop();

    for (SizeType i = 0; i < graph.Degree(v); ++i) {
      const auto &e = graph.EdgeFrom(v, i);
      if (!e.HasDirection(direction)) continue;

      WeightType td = d + e.PersonalizedCost(preference);
      VertexType tv = e.To();
      if (td < pot[tv]) {
        pot[tv] = td;
        par[tv] = make_pair(v, &e);
        que.Update(tv, td);
      }
    }
  }

  return pot;
}

vector<WeightType> DijkstraNormalUnitPreference(
    const Graph &graph, VertexType from, SizeType target_dimension, DirectionType direction) {
  VertexType num_vs = graph.NumVertices();
  vector<WeightType> pot(num_vs, kInfinityWeight);

  BinaryHeap que(graph.NumVertices());
  que.Reset();

  pot[from] = 0;
  que.Update(from, 0);
  assert(!que.IsEmpty());
  while (!que.IsEmpty()) {
    VertexType v;
    WeightType d;
    tie(v, d) = que.Pop();

    for (SizeType i = 0; i < graph.Degree(v); ++i) {
      const auto &e = graph.EdgeFrom(v, i);
      if (!e.HasDirection(direction)) continue;

      WeightType td = d + e.Cost()[target_dimension];
      VertexType tv = e.To();
      if (td < pot[tv]) {
        pot[tv] = td;
        que.Update(tv, td);
      }
    }
  }

  return pot;
}
}  // namespace multi_cost_graph
