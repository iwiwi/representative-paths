#include "alt_lower_bound.h"
#include "dijkstra.h"

DEFINE_int32(alt_num_landmarks, 10, "");
DEFINE_string(alt_strategy, "farthest", "");

namespace multi_cost_graph {
void ALTLowerBound::PrepareLandmarks(const Graph &graph, DirectionType direction, SizeType num_landmarks) {
  if (FLAGS_alt_strategy == "random") {
    PrepareLandmarksRandom(graph, direction, num_landmarks);
  } else if (FLAGS_alt_strategy == "farthest") {
    PrepareLandmarksFarthest(graph, direction, num_landmarks);
  }
}

void ALTLowerBound::PrepareLandmarksRandom(const Graph &graph, DirectionType direction, SizeType num_landmarks) {
  const SizeType num_dimensions = graph.NumDimensions();

  landmark_.assign(num_dimensions, vector<VertexType>(num_landmarks));
  distance_.assign(graph.NumVertices(),
                   vector<vector<WeightType>>(num_dimensions, vector<WeightType>(num_landmarks)));

  for (SizeType d = 0; d < num_dimensions; ++d) {
    for (SizeType i = 0; i < num_landmarks; ++i) {
      VertexType landmark = rand() % graph.NumVertices();

      vector<WeightType> ds =
          DijkstraNormalUnitPreference(graph, landmark, d, ReverseDirection(direction));

      for (VertexType v = 0; v < graph.NumVertices(); ++v) {
        distance_[v][d][i] = ds[v];
      }
    }
  }
}

void ALTLowerBound::PrepareLandmarksFarthest(const Graph &graph, DirectionType direction, SizeType num_landmarks) {
  const SizeType num_dimensions = graph.NumDimensions();

  landmark_.assign(num_dimensions, vector<VertexType>(num_landmarks));
  distance_.assign(graph.NumVertices(),
                   vector<vector<WeightType>>(num_dimensions, vector<WeightType>(num_landmarks)));

  for (SizeType d = 0; d < num_dimensions; ++d) {
    vector<WeightType> sum(graph.NumVertices());
    sum = DijkstraNormalUnitPreference(graph, rand() % graph.NumVertices(), d, direction);

    for (SizeType i = 0; i < num_landmarks; ++i) {
      VertexType landmark = max_element(all(sum)) - sum.begin();

      vector<WeightType> ds =
          DijkstraNormalUnitPreference(graph, landmark, d, ReverseDirection(direction));

      for (VertexType v = 0; v < graph.NumVertices(); ++v) {
        distance_[v][d][i] = ds[v];
        if (ds[v] != kInfinityWeight) sum[v] += ds[v];
      }
    }
  }
}
}  // namespace multi_cost_graph
