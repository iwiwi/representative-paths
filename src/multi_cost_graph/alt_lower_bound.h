#pragma once
#include "multi_cost_graph.h"
#include "common/common.h"

DECLARE_int32(alt_num_landmarks);
DECLARE_string(alt_strategy);

namespace multi_cost_graph {
class ALTLowerBound {
 public:
  // FLAGS_alt_strategy
  void PrepareLandmarks(const Graph &graph, DirectionType direction,
      SizeType num_landmarks = FLAGS_alt_num_landmarks);

  void PrepareLandmarksRandom(const Graph &graph, DirectionType direction,
      SizeType num_landmarks = FLAGS_alt_num_landmarks);

  void PrepareLandmarksFarthest(const Graph &graph, DirectionType direction,
      SizeType num_landmarks = FLAGS_alt_num_landmarks);

  inline WeightType LowerBound(VertexType from, VertexType to, SizeType d) const {
    const auto &d_from = distance_[from][d], &d_to = distance_[to][d];
    assert(d_from.size() == d_to.size());

    WeightType lb = 0.0;
    for (SizeType i = 0; i < d_from.size(); ++i) {
      if (d_to[i] == kInfinityWeight) continue;
      if (d_from[i] == kInfinityWeight) return kInfinityWeight;
      lb = max(lb, d_from[i] - d_to[i]);
    }
    return lb;
  }

  inline WeightType LowerBound(VertexType from, VertexType to, const WeightVector &w) const {
    assert(w.size() == landmark_.size());

    WeightType lb = 0.0;
    for (SizeType d = 0; d < landmark_.size(); ++d) {
      const WeightType x = LowerBound(from, to, d);
      if (x == kInfinityWeight) return kInfinityWeight;
      lb += x * w[d];
    }
    return lb;
  }

 private:
  // landmark_[d][i] := |i|-th landmark for |d|-th dimension
  vector<vector<VertexType>> landmark_;

  // distance_[v][d][i] := distance from |v| to |i|-th landmark for |d|-th dimension
  vector<vector<vector<WeightType>>> distance_;
};
}  // multi_cost_graph
