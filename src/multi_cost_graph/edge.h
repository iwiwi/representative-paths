#pragma once
#include "types.h"
#include "common/common.h"

namespace multi_cost_graph {
class Edge {
 public:
  inline VertexType To() const {
    return to_;
  }

  inline bool HasForward() const {
    return has_fwd_;
  }

  inline bool HasBackward() const {
    return has_bwd_;
  }

  inline bool HasDirection(DirectionType d) const {
    return d == kDirectionForward ? has_fwd_ : has_bwd_;
  }

  inline const WeightStorageType *Cost() const {
    return cost_;
  }

  // TODO: bad naming...
  inline WeightVector AddCost(WeightVector a) const {
    for (SizeType d = 0; d < a.size(); ++d) a[d] += cost_[d];
    return a;
  }

  inline WeightType PersonalizedCost(const WeightVector &preference) const {
    WeightType c = 0.0;
    for (SizeType d = 0; d < preference.size(); ++d) {
      c += cost_[d] * preference[d];
    }
    return c;
  }

  inline void Print(ostream &os = cerr, SizeType num_dimensions = 0) const {
    os << (has_fwd_ && has_bwd_ ? "=" : has_fwd_ ? ">" : has_bwd_ ? "<" : "?");
    os << to_;
    if (num_dimensions) {
      os << " (";
      for (SizeType d = 0; d < num_dimensions; ++d) {
        os << cost_[d] << (d + 1 == num_dimensions ? ")" : ", ");
      }
    }
  }

 private:
  friend class Graph;

  static constexpr SizeType kNumVertexBits = sizeof(VertexType) * CHAR_BIT - 2;

  Edge() : to_(0), has_fwd_(false), has_bwd_(false) {}
  ~Edge() {}

  union {
    struct {
      VertexType to_ : kNumVertexBits;
      bool has_fwd_ : 1;
      bool has_bwd_ : 1;
    };

    VertexType to_fwd_bwd_;  // Only for serialization
  };

  WeightStorageType cost_[1];
};

static_assert(sizeof(Edge) == sizeof(VertexType) + sizeof(WeightStorageType),
              "The size of class Edge is unexpected.");
}  // namespace multi_cost_graph
