#pragma once
#include "multi_cost_graph.h"

namespace multi_cost_graph {
vector<WeightType> Dijkstra(const Graph &graph, VertexType from,
                            const WeightVector &preference,
                            DirectionType direction);

vector<WeightType> DijkstraNormalUnitPreference(
    const Graph &graph, VertexType from, const SizeType target_dimension, DirectionType direction);
}  // multi_cost_graph
