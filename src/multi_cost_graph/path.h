#pragma once
#include "types.h"
#include "graph.h"
#include "edge.h"
#include "common/common.h"

namespace multi_cost_graph {
class Path {
 public:
  Path() : vertices_(), cost_() {};

  Path(const vector<VertexType> &vertices, const WeightVector &cost)
    : vertices_(vertices), cost_(cost) {}

  inline VertexType From() const {
    return vertices_[0];
  }

  inline VertexType To() const {
    return vertices_.back();
  }

  inline SizeType NumVertices() const {
    return vertices_.size();
  }

  inline VertexType VertexByOrder(SizeType i) const {
    return vertices_[i];
  }

  inline const WeightVector &Cost() const {
    return cost_;
  }

  inline WeightType PersonalizedCost(const WeightVector &preference) const {
    return Dot(cost_, preference);
  }

  inline void Reverse() {
    reverse(all(vertices_));
  }

  inline void Append(const Edge &e) {
    vertices_.emplace_back(e.To());
    cost_ = Add(cost_, e.Cost());
  }

  inline void Concatenate(const Path &path) {
    assert(To() == path.From());
    vertices_.insert(vertices_.end(), path.vertices_.begin() + 1, path.vertices_.end());
    cost_ = ::Add(cost_, path.Cost());
  }

  void Print(ostream &os = cerr) const {
	for (auto v : vertices_) {
		os << v << "->";
	}
    os << " --- ";
    PrintWeightVector(cost_, os);
    os << endl;
  }

  inline bool operator ==(const Path &a) {
    return vertices_ == a.vertices_;
  }

 private:
  vector<VertexType> vertices_;
  WeightVector cost_;
};

inline bool operator < (const Path &a, const Path &b) {
  return a.Cost() < b.Cost();
}
}  // namespace multi_cost_graph
