#include "graph.h"
#include "common/common.h"
#include "gtest/gtest.h"
using namespace multi_cost_graph;

TEST(Graph, Print) {
  Graph g;
  g.Construct({
    make_tuple(0, 1, WeightVector({1.0, 2.0})),
    make_tuple(1, 0, WeightVector({1.0, 2.0})),
    make_tuple(1, 2, WeightVector({2.5, 1.5})),
  });
  g.Print();
}

TEST(Graph, Construct) {
  const SizeType kNumTrials = 100;
  const SizeType kNumBidirectionalRoads = 100;
  const SizeType kNumUnidirectionlRoads = 100;
  const SizeType kNumVertices = 10;
  const SizeType kNumDimensions = 5;

  for (SizeType trial = 0; trial < kNumTrials; ++trial) {
    vector<Graph::EdgeTupleType> edges;

    for (SizeType i = 0; i < kNumBidirectionalRoads; ++i) {
      VertexType u, v;
      do {
        u = rand() % kNumVertices;
        v = rand() % kNumVertices;
      } while (u == v);
      const WeightVector c = RandomWeightVectorRandomLength(kNumDimensions);

      edges.emplace_back(u, v, c);
      edges.emplace_back(v, u, c);
    }

    for (SizeType i = 0; i < kNumUnidirectionlRoads; ++i) {
      VertexType u, v;
      do {
        u = rand() % kNumVertices;
        v = rand() % kNumVertices;
      } while (u == v);
      const WeightVector c = RandomWeightVectorRandomLength(kNumDimensions);

      edges.emplace_back(u, v, c);
    }

    random_shuffle(all(edges));

    Graph g;
    g.Construct(move(edges));

    ASSERT_EQ((kNumBidirectionalRoads + kNumUnidirectionlRoads) * 2,
              g.NumRepresentingEdges());
    ASSERT_EQ(kNumBidirectionalRoads * 2, g.NumBidirectionalEdges());
    ASSERT_EQ(kNumBidirectionalRoads * 2 + kNumUnidirectionlRoads,
              g.NumEdges(DirectionType::kDirectionBackward));
    ASSERT_EQ(kNumBidirectionalRoads * 2 + kNumUnidirectionlRoads,
              g.NumEdges(DirectionType::kDirectionForward));
  }
}

TEST(Graph, Echo) {
  Graph g;
  g.Construct({
    make_tuple(0, 1, WeightVector({1.0, 2.0})),
    make_tuple(1, 0, WeightVector({1.0, 2.0})),
    make_tuple(1, 2, WeightVector({2.5, 1.5})),
    make_tuple(2, 0, WeightVector({1.3, 2.5})),
  });
  g.Print();

  ostringstream oss1;
  g.Write(oss1);

  {
    istringstream iss(oss1.str());
    g.Read(iss);
    ostringstream oss2;
    g.Write(oss2);
    ASSERT_EQ(oss1.str(), oss2.str());
  }

  {
    Graph g2;
    istringstream iss(oss1.str());
    g2.Read(iss);
    ostringstream oss2;
    g2.Write(oss2);
    ASSERT_EQ(oss1.str(), oss2.str());
  }
}
