#pragma once
#include <cstdint>
#include "common/common.h"

namespace multi_cost_graph {
typedef int32_t VertexType;
typedef float WeightStorageType;

enum DirectionType {
  kDirectionForward = 0,
  kDirectionBackward = 1
};

inline DirectionType ReverseDirection(DirectionType direction) {
  return static_cast<DirectionType>(1 - static_cast<int>(direction));
}

static constexpr VertexType kDummyVertex = -1;

inline WeightVector Add(WeightVector a, const WeightStorageType *b) {
  for (SizeType i = 0; i < a.size(); ++i) {
    a[i] += b[i];
  }
  return a;  // move
}
}  // namespace multi_cost_graph
