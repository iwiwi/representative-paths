#include "alt_lower_bound.h"
#include "dijkstra.h"
#include "common/test.h"
#include "gtest/gtest.h"
using namespace multi_cost_graph;

TEST(ALTLowerBound, Random) {
  const SizeType kNumGraphs = 100;
  const SizeType kNumSources = 10;

  for (SizeType i = 0; i < kNumGraphs; ++i) {
    Graph g;
    ConstructRandomGraph(&g);

    ALTLowerBound alt;
    alt.PrepareLandmarks(g, kDirectionForward);

    for (SizeType j = 0; j < kNumSources; ++j) {
      const VertexType s = rand() % g.NumVertices();
      const WeightVector w = RandomWeightVectorUnitLength(g.NumDimensions());

      vector<WeightType> ds = Dijkstra(g, s, w, kDirectionForward);
      for (VertexType v = 0; v < g.NumVertices(); ++v) {
        ASSERT_FALSE(IsLessThan(ds[v], alt.LowerBound(s, v, w)));
      }
    }
  }
}
