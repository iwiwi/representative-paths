#pragma once
#include "types.h"
#include "common/common.h"

namespace multi_cost_graph {
class Edge;

class Graph {
 public:
  typedef tuple<VertexType, VertexType, WeightVector> EdgeTupleType;
  void Construct(vector<EdgeTupleType> edges);

  void Read(const char *file_path);
  void Read(istream &is);

  // Read the graph, and modify its dimension to |num_dimension| if necessary.
  // If the original dimension > |num_dimension|, then truncates the last dimensions.
  // If the original dimension < |num_dimension|, then adds random values.
  void ReadWithSpecifiedDimension(const char *file_path, SizeType num_dimensions);
  void ReadWithSpecifiedDimension(istream &is, SizeType num_dimensions);

  void Write(const char *file_path) const;
  void Write(ostream &os) const;

  void PrintSummary(ostream &os = cerr) const;
  void Print(ostream &os = cerr) const;
  void PrintTSV(ostream &os = cerr) const;

  inline VertexType NumVertices() const {
    return num_vertices_;
  }

  inline SizeType NumDimensions() const {
    return num_dimensions_;
  }

  SizeType NumEdges(DirectionType direction = kDirectionForward) const;
  SizeType NumBidirectionalEdges() const;

  inline SizeType NumRepresentingEdges() const {
    return index_.back();
  }

  inline SizeType Degree(VertexType v) const {
    return index_[v + 1] - index_[v];
  }

  inline const Edge &EdgeFrom(VertexType v, SizeType i) const {
    return EdgeByID(index_[v] + i);
  }

  SizeType Hash() const;  // For |FlyweightFactory|

 private:
  VertexType num_vertices_;
  SizeType num_dimensions_;
  vector<SizeType> index_;
  unique_ptr<uint8_t[]> edge_area_;

  inline Edge &EdgeByID(SizeType i) const {
    return *reinterpret_cast<Edge*>(&edge_area_[i * EdgeSize()]);
  }

  inline SizeType EdgeSize() const {
    // Be careful about alignment!
    return sizeof(VertexType) + sizeof(WeightStorageType) * num_dimensions_;
  }

  void ReadInternal(const char *file_path, bool is_dimension_specified, SizeType num_specified_dimensions);
  void ReadInternal(istream &is, bool is_dimension_specified, SizeType num_specified_dimensions);
};
}  // multi_cost_graph
