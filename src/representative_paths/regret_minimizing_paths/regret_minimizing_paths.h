#pragma once
#include "representative_paths/representative_paths.h"
#include "shortest_path_oracle/shortest_path_oracle.h"

DECLARE_bool(regret_minimizing_paths_init_single);

namespace representative_paths {
namespace regret_minimizing_paths {
class Base : public RepresentativePathsInterface {
 public:
  explicit Base(const multi_cost_graph::Graph &graph)
  : RepresentativePathsInterface(graph),
    shortest_path_oracle_(shortest_path_oracle::ShortestPathOracle(graph)) {}

  virtual ~Base() {}

  virtual void Precompute() {
    // shortest_path_oracle_->PrecomputeGlobal();  // Done in |FlyweightFactory|
  }

  virtual vector<multi_cost_graph::Path> Query(
      multi_cost_graph::VertexType from, multi_cost_graph::VertexType to, SizeType num_paths) = 0;

  virtual void ResetStatistics() {
    shortest_path_oracle_->ResetStatistics();
    stat_.Reset();
  }

  virtual void OutputStatistics() const {
    shortest_path_oracle_->OutputStatistics();
    stat_.Output();
  }

  virtual const char *RepresentativePathsName() const {
    return "Regret-minimizing";
  }

 protected:
  shared_ptr<shortest_path_oracle::OracleInterface> shortest_path_oracle_;

  struct Stat {
    Stat() {
      Reset();
    };

    void Reset() {
      num_generated_faces = 0;
    }

    void Output() const {
      JLOG_OPEN("regret_minimizing") {
        JLOG_PUT("num_generated_faces", num_generated_faces);
      }
    }

    SizeType num_generated_faces;
  } stat_;
};

class Naive : public Base {
 public:
  explicit Naive(const multi_cost_graph::Graph &graph) : Base(graph) {}
  virtual ~Naive() {}
  virtual vector<multi_cost_graph::Path> Query(
      multi_cost_graph::VertexType from, multi_cost_graph::VertexType to, SizeType num_paths);

  virtual const char *AlgorithmName() const {
    return "Naive";
  }
};

class Dynamic : public Base {
 public:
  explicit Dynamic(const multi_cost_graph::Graph &graph) : Base(graph) {}
  virtual ~Dynamic() {}
  virtual vector<multi_cost_graph::Path> Query(
      multi_cost_graph::VertexType from, multi_cost_graph::VertexType to, SizeType num_paths);

  virtual const char *AlgorithmName() const {
    return "Dynamic";
  }
};

class Lazy : public Base {
 public:
  explicit Lazy(const multi_cost_graph::Graph &graph) : Base(graph) {}
  virtual ~Lazy() {}

  // Answer representative paths
  virtual vector<multi_cost_graph::Path> Query(
      multi_cost_graph::VertexType from, multi_cost_graph::VertexType to, SizeType num_paths);

  virtual const char *AlgorithmName() const {
    return "Lazy";
  }
};
}  // namespace regret_minimizing_paths

typedef regret_minimizing_paths::Lazy RegretMinimizingPaths;
}  // namespace representative_paths
