#include "regret_minimizing_paths.h"
#include "geometry/geometry.h"
#include "regret/regret.h"
using namespace multi_cost_graph;
using namespace geometry;

namespace representative_paths {
namespace regret_minimizing_paths {
vector<Path> Dynamic::Query(VertexType from, VertexType to, SizeType num_paths) {
  shortest_path_oracle_->ResetQueryPoints(from, to);
  const SizeType num_dimensions = shortest_path_oracle_->NumDimensions();

  if (from == to) {
    return vector<Path>(1, Path({from}, WeightVector(num_dimensions, 0.0)));
  }

  // paths_costs[i] := paths[i].Cost()
  vector<Path> paths;
  vector<WeightVector> paths_costs;

  const SizeType num_first_vectors = FLAGS_regret_minimizing_paths_init_single ?
      1 : min(num_dimensions, num_paths);
  for (SizeType i = 0; i < num_first_vectors; ++i) {
    WeightVector preference = NormalUnitWeightVector(num_dimensions, i);
    Path p;
    CHECK(shortest_path_oracle_->Query(preference, &p) != kInfinityWeight);
    if (find(paths.begin(), paths.end(), p) != paths.end()) continue;
    paths.emplace_back(p);
    paths_costs.emplace_back(paths.back().Cost());
  }

  if (paths.size() >= num_paths) {
    return paths;
  }

  // current_cost[i]  := min(paths[j].PersonalizedCost(planes[i].normal_vector)
  // optimal_paths[i] := oracle->Query(planes[i].normal_vector)
  vector<Plane> planes(geometry::LowerEnvelope(paths_costs));
  vector<WeightType> planes_current_costs(planes.size());
  vector<WeightType> planes_optimal_costs(planes.size());
  vector<Path> planes_optimal_paths(planes.size());

  for (SizeType i = 0; i < planes.size(); ++i) {
    const WeightVector &preference = planes[i].normal_vector;
    {
      WeightType c = kInfinityWeight;
      for (const auto &path : paths) c = min(c, path.PersonalizedCost(preference));
      planes_current_costs[i] = c;
    }
    {
      WeightType c = shortest_path_oracle_->Query(planes[i].normal_vector, &planes_optimal_paths[i]);
      CHECK(c != kInfinityWeight);
      planes_optimal_costs[i] = c;
    }
  }

  while (paths.size() < num_paths) {
    // Select worst one from |candidate_paths|
    WeightType worst_regret = -1.0;
    SizeType worst_plane = kInfinitySize;
    for (SizeType i = 0; i < planes.size(); ++i) {
      const WeightType current = planes_current_costs[i];
      const WeightType optimal = planes_optimal_costs[i];
      const WeightType regret = regret::RegretFromCosts(current, optimal);
      if (regret > worst_regret) {
        worst_regret = regret;
        worst_plane = i;
      }
    }
    assert(worst_plane != kInfinitySize);

    // Add |worst_plane|-th plane
    const Path new_path = planes_optimal_paths[worst_plane];
    const WeightVector new_point = planes_optimal_paths[worst_plane].Cost();  // Reference would be invalid

    // Duplicated?
    {
      bool is_duplicated = false;
      for (SizeType i = 0; i < paths.size(); ++i) {
        if (IsEqual(paths[i].Cost(), new_path.Cost())) {
          is_duplicated = true;
          break;
        }
      }
      if (is_duplicated) break;
    }

    paths.emplace_back(new_path);
    paths_costs.emplace_back(new_point);
    if (paths.size() >= num_paths) break;

    vector<Plane> new_planes = AddPointToLowerEnvelope(paths_costs, new_point, planes);

    // Filter and update previous planes
    for (SizeType i = 0; i < planes.size(); ) {
      if (planes[i].Position(new_point) >= 0) {
        const WeightVector &preference = planes[i].normal_vector;
        planes_current_costs[i] =
            min(planes_current_costs[i], new_path.PersonalizedCost(preference));
        ++i;
      } else {
        if (i + 1 < planes.size()) {
          swap(planes              [i], planes              .back());
          swap(planes_current_costs[i], planes_current_costs.back());
          swap(planes_optimal_costs[i], planes_optimal_costs.back());
          swap(planes_optimal_paths[i], planes_optimal_paths.back());
        }
        planes.pop_back();
        planes_current_costs.pop_back();
        planes_optimal_costs.pop_back();
        planes_optimal_paths.pop_back();
      }
    }

    // Add new planes
    {
      SizeType i = planes.size();
      planes.insert(planes.end(), all(new_planes));
      planes_current_costs.resize(planes.size());
      planes_optimal_costs.resize(planes.size());
      planes_optimal_paths.resize(planes.size());
      for (; i < planes.size(); ++i) {
        const WeightVector &preference = planes[i].normal_vector;
        {
          WeightType c = kInfinityWeight;
          for (const auto &path : paths) c = min(c, path.PersonalizedCost(preference));
          planes_current_costs[i] = c;
        }
        {
          WeightType c = shortest_path_oracle_->Query(planes[i].normal_vector, &planes_optimal_paths[i]);
          CHECK(c != kInfinityWeight);
          planes_optimal_costs[i] = c;
        }
      }
    }
  }

  stat_.num_generated_faces += planes.size();
  return paths;
}
}  // namespace regret_minimizing_paths
}  // namespace representative_paths
