#include "regret_minimizing_paths.h"
#include "common/test.h"
#include "gtest/gtest.h"
using namespace multi_cost_graph;
using namespace representative_paths;

namespace {
void PrintPaths(const vector<Path> &paths) {
  cout << "==" << endl;
  for (const auto &path : paths) {
    PrintWeightVector(path.Cost(), cout);
    cout << endl;
  }
}
}  // namespace

TEST(RegretMinimizingPaths, Random) {
  const SizeType kNumGraphs = 100;
  const SizeType kNumPairs = 10;
  const SizeType kNumPaths = 10;

  for (SizeType i = 0; i < kNumGraphs; ++i) {
    Graph g;
    ConstructRandomGraph(&g);

    vector<unique_ptr<RepresentativePathsInterface>> algorithms;
    algorithms.emplace_back(new regret_minimizing_paths::Naive(g));
    algorithms.emplace_back(new regret_minimizing_paths::Dynamic(g));
    algorithms.emplace_back(new regret_minimizing_paths::Lazy(g));

    for (const auto &o : algorithms) o->Precompute();

    for (SizeType j = 0; j < kNumPairs; ++j) {
      const VertexType s = rand() % g.NumVertices();
      const VertexType t = rand() % g.NumVertices();
      if (s == t) continue;

      vector<Path> paths0 = algorithms[0]->Query(s, t, kNumPaths);

      for (const auto &o : algorithms) {
        vector<Path> paths1 = o->Query(s, t, kNumPaths);
        PrintPaths(paths1);

        ASSERT_EQ(paths0.size(), paths1.size()) << o->AlgorithmName();

        for (SizeType i = 0; i < paths0.size(); ++i) {
          ASSERT_TRUE(IsEqual(paths0[i].Cost(), paths1[i].Cost()));
        }
      }
    }
  }
}
