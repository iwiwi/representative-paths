#include "regret_minimizing_paths.h"
#include "geometry/geometry.h"
#include "regret/regret.h"
using namespace multi_cost_graph;
using namespace geometry;

namespace representative_paths {
namespace regret_minimizing_paths {
vector<multi_cost_graph::Path> Naive::Query(
    VertexType from, VertexType to, SizeType num_paths) {
  const SizeType num_dimensions = shortest_path_oracle_->NumDimensions();

  if (from == to) {
    return vector<Path>(1, Path({from}, WeightVector(num_dimensions, 0.0)));
  }
  shortest_path_oracle_->ResetQueryPoints(from, to);

  vector<multi_cost_graph::Path> paths;

  const SizeType num_first_vectors = FLAGS_regret_minimizing_paths_init_single ?
      1 : min(num_dimensions, num_paths);
  for (SizeType i = 0; i < num_first_vectors; ++i) {
    WeightVector preference = NormalUnitWeightVector(num_dimensions, i);
    Path p;
    CHECK(shortest_path_oracle_->Query(preference, &p) != kInfinityWeight);
    if (find(paths.begin(), paths.end(), p) != paths.end()) continue;
    paths.emplace_back(p);
  }

  while (paths.size() < num_paths) {
    regret::RegretQuery regret_query(shortest_path_oracle_, paths);
    WeightVector worst_preference;
    RegretExact(regret_query, &worst_preference);
    assert(worst_preference.size() == shortest_path_oracle_->NumDimensions());

    paths.emplace_back();
    CHECK(shortest_path_oracle_->Query(worst_preference, &paths.back()) != kInfinityWeight);

    bool is_duplicated = false;
    for (SizeType i = 0; i + 1 < paths.size(); ++i) {
      if (IsEqual(paths[i].Cost(), paths.back().Cost())) {
        is_duplicated = true;
        break;
      }
    }
    if (is_duplicated) {
      paths.pop_back();
      break;
    }
  }

  return paths;
}
}  // namespace regret_minimizing_paths
}  // namespace representative_paths
