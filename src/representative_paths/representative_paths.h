#pragma once
#include "multi_cost_graph/multi_cost_graph.h"

DECLARE_string(representative_paths);

namespace representative_paths {
class RepresentativePathsInterface {
 public:
  explicit RepresentativePathsInterface(const multi_cost_graph::Graph &graph) : graph_(graph) {}
  virtual ~RepresentativePathsInterface() {}

  virtual void Precompute() = 0;
  virtual vector<multi_cost_graph::Path> Query(
      multi_cost_graph::VertexType from, multi_cost_graph::VertexType to, SizeType num_paths) = 0;

  virtual void ResetStatistics() {}
  virtual void OutputStatistics() const {}

  virtual const char *RepresentativePathsName() const = 0;
  virtual const char *AlgorithmName() const = 0;

 protected:
  const multi_cost_graph::Graph &graph_;
};

// Reads |--representative_paths| flag (variable |FLAGS_representative_paths|)
RepresentativePathsInterface *RepresentativePaths(const multi_cost_graph::Graph &graph);

RepresentativePathsInterface *RepresentativePathsFromName(
    const string &name, const multi_cost_graph::Graph &graph);
}  // namespace representative_paths
