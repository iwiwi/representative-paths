#include "representative_paths.h"
#include "regret_minimizing_paths/regret_minimizing_paths.h"
#include "skyline_paths.h"
#include "top_k_paths.h"
#include "random_paths.h"
using namespace multi_cost_graph;

DEFINE_string(representative_paths, "regret_minimizing", "");

namespace representative_paths {
RepresentativePathsInterface *RepresentativePaths(const Graph &graph) {
  return RepresentativePathsFromName(FLAGS_representative_paths, graph);
}

RepresentativePathsInterface *RepresentativePathsFromName(const string &name,
                                                          const Graph &graph) {
  if (name == "regret_minimizing") {
    return new RegretMinimizingPaths(graph);
  } else if (name == "skyline") {
    return new SkylinePaths(graph);
  } else if (name == "top_k") {
    return new TopKPaths(graph);
  } else if (name == "random") {
    return new RandomPaths(graph);
  }

  if (name == "regret_minimizing_naive") {
    return new regret_minimizing_paths::Naive(graph);
  } else if (name == "regret_minimizing_dynamic") {
    return new regret_minimizing_paths::Dynamic(graph);
  } else if (name == "regret_minimizing_lazy") {
    return new regret_minimizing_paths::Lazy(graph);
  }

  if (name == "skyline_naive") {
    return new skyline_paths::NaiveLabelCorrectingSearch(graph);
  } else if (name == "skyline_pp") {
    return new skyline_paths::ParetoPrep(graph);
  }


  CHECK(!"Unknown algorithm specified");
  return nullptr;
}
}  // namsepace representative_paths
