#pragma once
#include "representative_paths.h"

namespace representative_paths {
class RandomPaths : public RepresentativePathsInterface {
 public:
  explicit RandomPaths(const multi_cost_graph::Graph &graph)
  : RepresentativePathsInterface(graph),
    shortest_path_oracle_(shortest_path_oracle::ShortestPathOracle(graph)) {}
  virtual ~RandomPaths() {}

  virtual void Precompute() {
    shortest_path_oracle_->PrecomputeGlobal();
  }

  virtual vector<multi_cost_graph::Path> Query(
      multi_cost_graph::VertexType from, multi_cost_graph::VertexType to, SizeType num_paths) {
    const SizeType kNumTrials = 30;
    rnd_ = common::Random();  // For consistent results

    shortest_path_oracle_->ResetQueryPoints(from, to);
    vector<multi_cost_graph::Path> paths;
    for (SizeType i = 0; i < num_paths; ++i) {
      // The limit of the number of trials is necessary because
      // the number of skyline paths can be less than |num_paths|.
      for (SizeType trial = 0; trial < kNumTrials; ++trial) {
        WeightVector preference = MyRandomWeightVectorUnitLength(graph_.NumDimensions());
        multi_cost_graph::Path p;
        shortest_path_oracle_->Query(preference, &p);
        if (find(paths.begin(), paths.end(), p) == paths.end()) {
          paths.emplace_back(p);
          break;
        }
      }
    }
    assert(paths.size() <= num_paths);
    return paths;
  }

  virtual const char *RepresentativePathsName() const {
    return "Random";
  }

  virtual const char *AlgorithmName() const {
    return "Random";
  }

 private:
  shared_ptr<shortest_path_oracle::OracleInterface> shortest_path_oracle_;

  common::Random rnd_;

  inline WeightVector MyRandomWeightVectorUnitLength(SizeType num_dimensions) {
    WeightVector a(num_dimensions);
    for (SizeType i = 0; i < num_dimensions; ++i) a[i] = rnd_.Next() / WeightType(rnd_.Max());
    WeightType l = Abs(a);
    for (SizeType i = 0; i < num_dimensions; ++i) a[i] /= l;
    return a;
  }
};
}  // namespace representative_paths
