#pragma once
#include "representative_paths.h"
#include "multi_cost_graph/multi_cost_graph.h"
#include "common/binary_heap.h"

namespace representative_paths {
namespace skyline_paths {
class RecursivePath {
 public:
  RecursivePath(multi_cost_graph::VertexType to, const WeightVector &cost, RecursivePath *parent)
  : to_(to), cost_(cost), parent_(parent) {}

  multi_cost_graph::VertexType To() const {
    return to_;
  }

  const WeightVector &Cost() const {
    return cost_;
  }

  multi_cost_graph::Path ConvertToPath() const {
    vector<multi_cost_graph::VertexType> vs;
    for (const RecursivePath *rp = this; rp != nullptr; rp = rp->parent_) {
      vs.emplace_back(rp->to_);
    }
    reverse(all(vs));
    return multi_cost_graph::Path(vs, Cost());
  }

  void Print(ostream &os = cerr) const {
    ConvertToPath().Print(os);
  }

 private:
  multi_cost_graph::VertexType to_;
  WeightVector cost_;
  RecursivePath *parent_;
};

// Label-correcting search algorithm (LCS)
class NaiveLabelCorrectingSearch : public RepresentativePathsInterface {
 public:
  explicit NaiveLabelCorrectingSearch(const multi_cost_graph::Graph &graph) : RepresentativePathsInterface(graph) {}
  virtual ~NaiveLabelCorrectingSearch() {}

  virtual void Precompute() {}
  virtual vector<multi_cost_graph::Path> Query(
      multi_cost_graph::VertexType from, multi_cost_graph::VertexType to, SizeType num_paths);

  virtual const char *RepresentativePathsName() const {
    return "Skyline";
  }

  virtual const char *AlgorithmName() const {
    return "Naive";
  }
};


// Michael Shekelyan, Gregor Jossé, Matthias Schubert.
// "ParetoPrep: Fast computation of Path Skylines Queries"
// http://arxiv.org/abs/1410.0205
class ParetoPrep : public RepresentativePathsInterface {
 public:
  typedef typename multi_cost_graph::VertexType VertexType;

  explicit ParetoPrep(const multi_cost_graph::Graph &graph)
  : RepresentativePathsInterface(graph),
    num_vertices_(graph.NumVertices()), num_dimensions_(graph.NumDimensions()),
    que_(num_vertices_), is_visited_(num_vertices_), visited_vertices_(),
    lower_bound_(num_vertices_, WeightVector(num_dimensions_, kInfinityWeight)),
    paths_(num_vertices_), num_done_paths_(num_vertices_) {}

  virtual ~ParetoPrep() {}

  virtual void Precompute() {}

  virtual vector<multi_cost_graph::Path> Query(
      multi_cost_graph::VertexType from, multi_cost_graph::VertexType to, SizeType num_paths);

  virtual const char *RepresentativePathsName() const {
    return "Skyline";
  }

  virtual const char *AlgorithmName() const {
    return "ParetoPrep";
  }

 protected:
  VertexType num_vertices_;
  SizeType num_dimensions_;

  BinaryHeap que_;
  vector<bool> is_visited_;
  vector<VertexType> visited_vertices_;

  vector<WeightVector> lower_bound_;
  vector<vector<unique_ptr<RecursivePath>>> paths_;
  vector<SizeType> num_done_paths_;

  void Visit(VertexType v) {
    if (is_visited_[v]) return;
    is_visited_[v] = true;
    visited_vertices_.emplace_back(v);

    assert(paths_[v].size() == 0);
    paths_[v].resize(num_dimensions_);
  }

  void Reset() {
    que_.Reset();
    for (VertexType v : visited_vertices_) {
      is_visited_[v] = false;
      lower_bound_[v].assign(num_dimensions_, kInfinityWeight);
      paths_[v].clear();
      num_done_paths_[v] = 0;
    }
    visited_vertices_.clear();
  }
};
}  // namespace skyline

typedef skyline_paths::ParetoPrep SkylinePaths;
}  // namespace representative_paths
