// This implementation is largely helped by Takanori Hayashi

#include "top_k_paths.h"
#include "common/common.h"

namespace representative_paths {
namespace top_k_paths {
namespace {
template <typename T>
struct edge{
  int from;
  int to;
  T cost;
  edge(){}
  edge(int from, int to, T cost) : from(from), to(to), cost(cost) {}
};

template <typename T>
struct Path{
  T cost;
  vector<int> nodes;
  Path(){}
  Path(T _cost, const vector<int> &_nodes) : cost(_cost), nodes(_nodes) {}
  void print() const{
    for(size_t i = 0; i < nodes.size(); i++)
      std::cout << nodes[i] << (i + 1 == nodes.size() ? '\n' : '-');
  }
  bool empty() const {return nodes.size() == 0; }
};

template <typename T>
bool operator<(const Path<T> &p1, const Path<T> &p2){
  return p1.cost > p2.cost;
}

template <typename T>
Path<T> make_path(int s, int t, const vector<T> &dist, const vector<int> &prev){
  vector<int> nodes;
  for(int v = t; s != v; v = prev[v]) nodes.push_back(v);
  nodes.push_back(s);
  std::reverse(nodes.begin(), nodes.end());
  return Path<T>(dist[t], nodes);
}

template <typename T>
Path<T> operator+(const Path<T> &p1, const Path<T> &p2){
  assert(!p1.empty() && !p2.empty() && p1.nodes.back() == p2.nodes[0]);
  vector<int> nodes = p1.nodes;
  for(size_t i = 1; i < p2.nodes.size(); i++) nodes.push_back(p2.nodes[i]);
  return Path<T>(p1.cost + p2.cost, nodes);
}

struct vertex{
  int v;
  int pre, post;
  vertex(){}
  vertex(int v, int pre = -1, int post = -1) : v(v), pre(pre), post(post) {}
  friend bool operator<(const vertex &v1, const vertex &v2){
    return v1.pre < v2.pre;
  }
};

template <typename T>
class Cikm10{
 public:
  typedef vector<vector<edge<T> > > Graph;

  Cikm10(const vector<edge<T> > &es);
  size_t best_paths(int s, int t, int k);
  Path<T> get_path(int k);

 private:
  const T inf;
  double exec_time;
  vector<T> spt_cost;
  vector<int>    spt_prev;

  vector<T> locate_cost;
  vector<int>    locate_prev;
  vector<int>    ignoredE;

  class PathTree{
    struct node_t{
      int v;
      std::map<int, node_t*> ch;
      node_t(int s) : v(s) {}
      ~node_t(){
        for(typename std::map<int, node_t*>::iterator iter = ch.begin();
            iter != ch.end(); iter++){
          delete iter->second;
        }
      }
    };
    node_t *root;
    std::map<T, int>  path_length_count;
    std::set<vector<int> > used_paths;

   public:

    PathTree(const Path<T> &p){
      assert(!p.empty());
      root = new node_t(p.nodes[0]);
      add_path(p);
      used_paths.clear();
    }

    ~PathTree(){ delete root; }
    void add_path(const Path<T> &p);
    bool find_path(const Path<T> &p);
    void generate_candidate_path(Cikm10<T> *GaoTopK,
                                 const Graph &graph, int s, int t,
                                 const Path<T> &p,
                                 const vector<vertex> &vs,
                                 std::priority_queue<Path<T> > &path_que);
    int get_path_length_count(const T &w){ return path_length_count[w]; }
  };


  Graph adj;
  vector<Path<T> > kshort;

  void dijkstra(const Graph &graph, int s, int t,
                vector<T> &cost, vector<int> &prev);
  Path<T> dijkstra(const Graph &graph, int s, int t);

  void dfs_vertex_labeling(const Graph &tree, int v, int &id, vector<vertex> &vs);

  vector<vertex> transform_graph(Graph &graph, int s, int t,
                                 vector<T> &cost, vector<int> &prev);
  void remove_descendents(vector<vertex> &ignored_node);

  inline bool path_found(const vertex &v, const vector<vertex> &ignored_nodes);

  Path<T> locate(const Graph &graph,
                 vector<vertex> IgN,
                 const vector<int> &IgE,
                 int d, int t,
                 const vector<vertex> &vs);

  Path<T> make_spt_path(int v, int t);

  double get_current_time_sec(){
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec + tv.tv_usec * 1e-6;
  }

};

template <typename T>
Cikm10<T>::
Cikm10(const vector<edge<T> > &es)
: inf(std::numeric_limits<T>::max()){
  int V = 0;
  for(size_t i = 0; i < es.size(); i++){
    V = std::max(V, std::max(es[i].to, es[i].from) + 1);
  }
  adj.resize(V);
  for(size_t i = 0; i < es.size(); i++){
    assert(es[i].from < V);
    adj[es[i].from].push_back(es[i]);
  }


  // init tables
  spt_cost.resize(adj.size());
  spt_prev.resize(adj.size());

  locate_cost = vector<T>(adj.size(), inf);
  locate_prev = vector<int>(adj.size(), -1);
  ignoredE    = vector<int>(adj.size(), 0);
};



template <typename T>
Path<T> Cikm10<T>::
get_path(int k){
  return k < (int)kshort.size() ? kshort[k] : Path<T>();
}

template <typename T>
void Cikm10<T>::
dijkstra(const Graph &graph, int s, int,
         vector<T> &cost, vector<int> &prev){
  std::fill(cost.begin(), cost.end(), inf);
  std::fill(prev.begin(), prev.end(), -1);

  // boost::heap::fibonacci_heap<std::pair<T, int> > que;
  std::priority_queue<std::pair<T, int> > que;

  cost[s] = 0;
  que.push(std::make_pair(0, s));

  while(!que.empty()){
    std::pair<T, int> p = que.top();  que.pop();
    T c = -p.first;
    int v = p.second;
    if(c > cost[v]) continue;
    for(size_t i = 0; i < graph[v].size(); i++){
      const edge<T> &e = graph[v][i];

      T tmp = e.cost + cost[v];

      if(tmp < cost[e.to]){
        cost[e.to] = tmp;
        prev[e.to] = v;
        que.push(make_pair(-tmp, e.to));
      }
    }
  }
}

template <typename T>
Path<T> Cikm10<T>::
dijkstra(const Graph &graph, int s, int t){
  vector<T> cost(graph.size(), inf);
  vector<int> prev(graph.size(), -1);
  dijkstra(graph, s, t, cost, prev);
  return cost[t] == inf ? Path<T>() : make_path(s, t, cost, prev);
}

template <typename T>
void Cikm10<T>::
dfs_vertex_labeling(const Graph &tree, int v, int &id, vector<vertex> &vs){
  vs[v].pre = id++;
  for (size_t i = 0; i < tree[v].size(); i++){
    dfs_vertex_labeling(tree, tree[v][i].to, id, vs);
  }
  vs[v].post = id++;
}

template <typename T>
vector<vertex> Cikm10<T>::
transform_graph(Graph &graph, int s, int t,
                vector<T> &cost, vector<int> &prev){
  int n = graph.size();
  Graph rgraph(n);

  for(int i = 0;i < n; i++) {
    for(size_t j = 0; j < graph[i].size(); j++){
      int to = graph[i][j].to;
      rgraph[to].push_back(edge<T>(to, i, graph[i][j].cost));
    }
  }

  dijkstra(rgraph, t, s, cost, prev);

  // change cost using potential
      for(int i = 0; i < n; i++){
        for(size_t j = 0; j < graph[i].size(); j++){
          if(prev[i] == graph[i][j].to){
            // Because of parallel edge, the following assertion fails falsely
            // assert(IsZero(graph[i][j].cost + cost[graph[i][j].to] - cost[i]));
          }
          graph[i][j] = edge<T>(i,
                                graph[i][j].to,
                                graph[i][j].cost + cost[graph[i][j].to] - cost[i]);
        }
      }

      vector<vertex> vs(n);
      for(int i = 0; i < n; i++) vs[i].v = i;

      // compute shortest paths tree
      Graph spt_in(n);
      for(size_t i = 0; i < graph.size(); i++){
        if(prev[i] != -1){
          spt_in[prev[i]].push_back(edge<T>(prev[i], i, 1));
        }
      }

      int id = 0;
      dfs_vertex_labeling(spt_in, t, id, vs);
      return vs;
}

template <typename T>
void Cikm10<T>::
remove_descendents(vector<vertex> &ignored_nodes){
  int m = 0;
  for(size_t i = 0; i < ignored_nodes.size(); i++){
    if(i == 0 || ignored_nodes[m-1].post < ignored_nodes[i].post){
      ignored_nodes[m++] = ignored_nodes[i];
    }
  }
  ignored_nodes.resize(m);
}
template <typename T>
inline bool Cikm10<T>::
path_found(const vertex &v, const vector<vertex> &ignored_nodes){
  if(ignored_nodes.size() == 0 || v.pre < ignored_nodes[0].pre) return true;
  int ub = ignored_nodes.size(), lb = 0;
  while(ub - lb > 1) {
    int mb = (lb + ub) / 2;
    if(v.pre < ignored_nodes[mb].pre) ub = mb;
    else lb = mb;
  }
  return v.post > ignored_nodes[lb].post;
}


template <typename T>
Path<T> Cikm10<T>::
locate(const Graph &graph,
       vector<vertex> IgN,
       const vector<int> &IgE,
       int d, int t,
       const vector<vertex> &vs){

  vector<T> &cost = locate_cost;
  vector<int>    &prev = locate_prev;

  vector<int> changed_idx;

  for(size_t i = 0; i < IgN.size(); i++){
    cost[IgN[i].v] = -inf;
    changed_idx.push_back(IgN[i].v);
  }

  for(size_t i = 0; i < IgE.size(); i++){
    ignoredE[IgE[i]] = 1;
  }

  sort(IgN.begin(), IgN.end());
  remove_descendents(IgN);

  cost[d] = 0;

  std::priority_queue<std::pair<T, int> > que;
  que.push(std::make_pair(0, d));

  Path<T> sp;

  while(!que.empty()){
    std::pair<T, int> p = que.top(); que.pop();
    int v = p.second;
    T v_cost = -p.first;

    if(v_cost > cost[v]) continue;

    if(spt_cost[v] != inf && (v == t || path_found(vs[v], IgN))){
      Path<T> spt_path = make_spt_path(v, t);
      assert(spt_path.cost == 0);
      sp = make_path(d, v, cost, prev) + spt_path;
      break;
    }

    for(size_t i = 0; i < graph[v].size(); i++){
      int to  = graph[v][i].to;
      T tmp = graph[v][i].cost + v_cost;
      if(v == d && ignoredE[to]) continue;

      if(tmp < cost[to]){
        cost[to] = tmp;
        prev[to] = v;
        changed_idx.push_back(to);
        que.push(make_pair(-tmp, to));
      }
    }
  }


  // init table again
  for(size_t i = 0; i < changed_idx.size(); i++){
    cost[changed_idx[i]] = inf;
    prev[changed_idx[i]] = -1;
  }

  for(size_t i = 0; i < IgE.size(); i++){
    ignoredE[IgE[i]] = 0;
  }

  return sp;
}

template <typename T>
void Cikm10<T>::PathTree::
add_path(const Path<T> &p){
  node_t *t = root;
  vector<int> ignored_nodes;

  path_length_count[p.cost]++;
  used_paths.insert(p.nodes);

  for(size_t i = 1; i < p.nodes.size(); i++){
    if(t->ch.find(p.nodes[i]) == t->ch.end()){
      t->ch[p.nodes[i]] = new node_t(p.nodes[i]);
    }
    t = t->ch[p.nodes[i]];
  }
}

template <typename T>
inline bool Cikm10<T>::PathTree::
find_path(const Path<T> &p){
  return used_paths.count(p.nodes) > 0;
}

template <typename T>
void Cikm10<T>::PathTree::
generate_candidate_path(Cikm10<T> *GaoTopK,
                        const Graph &graph, int, int t,
                        const Path<T> &p,
                        const vector<vertex> &vs,
                        std::priority_queue<Path<T> > &path_que){

  T sum = 0;
  node_t *pt_node = root;
  path_length_count[p.cost]--;

  vector<vertex> IgN;
  vector<int> cur_path;

  for(size_t i = 0; i + 1 < p.nodes.size(); i++){
    int v = p.nodes[i];
    vector<int> IgE;

    IgN.push_back(vs[v]);
    cur_path.push_back(v);

    for(typename std::map<int, node_t*>::iterator iter = pt_node->ch.begin();
        iter != pt_node->ch.end(); iter++){
      IgE.push_back(iter->first);
    }

    Path<T> candidate_path = GaoTopK->locate(graph, IgN, IgE,v,t,vs);

    if(!candidate_path.empty()){
      // std::cerr << candidate_path.cost << std::endl;
      candidate_path = Path<T>(sum, cur_path) + candidate_path;
      path_que.push(candidate_path);
    }

    pt_node = pt_node->ch[p.nodes[i+1]];

    for(size_t j = 0; j < graph[v].size(); j++){
      if(graph[v][j].to == p.nodes[i+1]) {
        sum += graph[v][j].cost;
      }
    }

  }
}

template <typename T>
Path<T> Cikm10<T>::
make_spt_path(int v, int t){
  Path<T> ans = make_path(t, v, spt_cost, spt_prev);
  ans.cost = 0;
  reverse(ans.nodes.begin(), ans.nodes.end());
  return ans;
}


template <typename T>
size_t Cikm10<T>::
best_paths(int s, int t, int k_) {
  int k = max(k_, 3);  // TODO

  exec_time = -get_current_time_sec();

  kshort.resize(k);

  Graph graph = adj;

  vector<vertex> vs = transform_graph(graph, s, t, spt_cost, spt_prev);

  Path<T> p = dijkstra(graph, s, t);
  if(p.empty()) return 0;

  PathTree pt(p);
  std::priority_queue<Path<T> > path_que;

  path_que.push(p);

  int pnum = 0;
  for(; pnum < k; pnum++){

    while(!path_que.empty() && pt.find_path(path_que.top())) path_que.pop();

    if(path_que.empty()) break;

    kshort[pnum] = path_que.top(); path_que.pop();
    pt.add_path(kshort[pnum]);

    // optimization strategy 1:
    // when we have already found top-k shortest paths, we can finish iteration.
    if(pt.get_path_length_count(kshort[pnum].cost) + pnum >= k){
      while(++pnum < k){
        kshort[pnum] = path_que.top();    path_que.pop();
      }
      break;
    }

    // std::cerr <<pnum<< "th path: "<<exec_time+get_current_time_sec() << std::endl;
    // std::cerr <<pnum<< "   cost: "<<kshort[pnum].cost << std::endl;
    // kshort[pnum].print();
    pt.generate_candidate_path(this,graph,s,t,kshort[pnum], vs,path_que);
  }

  exec_time += get_current_time_sec();
  // std::cerr << "the number of found paths: " << pnum <<  std::endl;
  // std::cerr << "execution time           : " << exec_time << std:: endl;

  return min(k_, pnum);
}
}  // namespace

vector<multi_cost_graph::Path> KReductionEager::Query(multi_cost_graph::VertexType from,
                                                multi_cost_graph::VertexType to,
                                                SizeType num_required_paths) {
  vector<edge<WeightType>> edges;
  for (multi_cost_graph::VertexType v = 0; v < graph_.NumVertices(); ++v) {
    for (SizeType i = 0; i < graph_.Degree(v); ++i) {
      const auto &edge = graph_.EdgeFrom(v, i);
      if (!edge.HasForward()) continue;
      edges.emplace_back(v, edge.To(), edge.Cost()[0]);
    }
  }

  Cikm10<WeightType> a(edges);
  SizeType num_obtained_paths = a.best_paths(from, to, num_required_paths);
  // assert(num_obtained_paths <= num_required_paths);

  vector<multi_cost_graph::Path> paths;
  for (SizeType i = 0; i < num_obtained_paths; ++i) {
    auto p = a.get_path(i);
    const auto &nodes = p.nodes;

    // WeightVector c = Mul(NormalUnitWeightVector(graph_.NumDimensions(), 0), p.cost);
    WeightVector c(graph_.NumDimensions(), 0.0);
    for (SizeType j = 0; j + 1 < nodes.size(); ++j) {
      multi_cost_graph::VertexType u = nodes[j], v = nodes[j + 1];

      WeightVector min_w(graph_.NumDimensions(), kInfinityWeight);
      for (SizeType k = 0; k < graph_.Degree(u); ++k) {
        const multi_cost_graph::Edge &e = graph_.EdgeFrom(u, k);
        if (e.To() != v || !e.HasForward()) continue;
        min_w = min(min_w, WeightVector(e.Cost(), e.Cost() + graph_.NumDimensions()));
      }
      c = Add(c, min_w);
    }

    PrintWeightVector(c); cerr << endl;
    // cout << p.cost << endl;

    paths.emplace_back(p.nodes, c);
    assert(paths.back().From() == from);
    assert(paths.back().To() == to);
    // assert(IsEqual(paths.back().Cost()[0], p.cost));
  }
  return paths;
}
}  // namespace top_k_paths
}  // namespace representative_paths
