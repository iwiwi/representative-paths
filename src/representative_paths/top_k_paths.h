#pragma once
#include "representative_paths/representative_paths.h"
#include "common/common.h"

namespace representative_paths {
namespace top_k_paths {
// Jun Gao, Huida Qiu, Xiao Jiang, Tengjiao Wang, and Dongqing Yang.
// Fast top-k simple shortest paths discovery in graphs. In CIKM'10.
class KReductionEager : public RepresentativePathsInterface {
 public:
  explicit KReductionEager(const multi_cost_graph::Graph &graph) :
                        RepresentativePathsInterface(graph) {}

  virtual ~KReductionEager() {}

  virtual void Precompute() {}

  virtual vector<multi_cost_graph::Path> Query(
      multi_cost_graph::VertexType from, multi_cost_graph::VertexType to, SizeType num_paths);

  virtual const char *RepresentativePathsName() const {
    return "Top-K";
  }

  virtual const char *AlgorithmName() const {
    return "KReductionEager";
  }
};
}  // namespace top_k_paths

typedef top_k_paths::KReductionEager TopKPaths;
}  // namesapce representative_paths
