#include "skyline_paths.h"
#include "gtest/gtest.h"
#include "common/common.h"
#include "common/test.h"
using namespace multi_cost_graph;

TEST(Skyline, Naive_Trivial) {
  Graph g;
  g.Construct({
    make_tuple(0, 1, WeightVector({1.0, 2.0})),
    make_tuple(0, 1, WeightVector({2.0, 1.0})),
    make_tuple(0, 1, WeightVector({2.0, 2.0})),
    make_tuple(1, 2, WeightVector({1.0, 2.0})),
    make_tuple(1, 2, WeightVector({2.0, 1.0})),
    make_tuple(1, 2, WeightVector({3.0, 3.0})),
  });

  representative_paths::skyline_paths::NaiveLabelCorrectingSearch algo(g);
  algo.Precompute();

  vector<Path> paths = algo.Query(0, 2, 0);
  sort(all(paths));

  for (const auto &path : paths) {
    path.Print();
  }

  ASSERT_EQ(paths.size(), 3U);
  ASSERT_TRUE(paths[0].Cost() == WeightVector({2, 4}));
  ASSERT_TRUE(paths[1].Cost() == WeightVector({3, 3}));
  ASSERT_TRUE(paths[2].Cost() == WeightVector({4, 2}));
}

TEST(Skyline, PP_Trivial) {
  Graph g;
  g.Construct({
    make_tuple(0, 1, WeightVector({1.0, 2.0})),
    make_tuple(0, 1, WeightVector({2.0, 1.0})),
    make_tuple(0, 1, WeightVector({2.0, 2.0})),
    make_tuple(1, 2, WeightVector({1.0, 2.0})),
    make_tuple(1, 2, WeightVector({2.0, 1.0})),
    make_tuple(1, 2, WeightVector({3.0, 3.0})),
  });

  representative_paths::skyline_paths::ParetoPrep algo(g);
  algo.Precompute();

  vector<Path> paths = algo.Query(0, 2, 0);
  sort(all(paths));

  for (const auto &path : paths) {
    path.Print();
  }

  ASSERT_EQ(paths.size(), 3U);
  ASSERT_TRUE(paths[0].Cost() == WeightVector({2, 4}));
  ASSERT_TRUE(paths[1].Cost() == WeightVector({3, 3}));
  ASSERT_TRUE(paths[2].Cost() == WeightVector({4, 2}));
}

TEST(Skyline, Random) {
  const SizeType kNumGraphs = 100;
  const SizeType kNumPairs = 30;
  for (SizeType i = 0; i < kNumGraphs; ++i) {
    Graph g;
    ConstructRandomGraph(&g);

    representative_paths::skyline_paths::NaiveLabelCorrectingSearch a1(g);
    representative_paths::skyline_paths::ParetoPrep a2(g);

    a1.Precompute();
    a2.Precompute();
    cout << "Skylines:";
    for (SizeType i = 0; i < kNumPairs; ++i) {
      VertexType s = rand() % g.NumVertices();
      VertexType t = rand() % g.NumVertices();

      vector<Path> p1 = a1.Query(s, t, 0);
      vector<Path> p2 = a2.Query(s, t, 0);

      if (false) {
        cout << "====" << endl;
        for (const auto &p : p1) p.Print(cout);
        cout << "--" << endl;
        for (const auto &p : p2) p.Print(cout);
      }

      ASSERT_EQ(p1.size(), p2.size());
      cout << " " << p1.size();
    }
    cout << endl;
  }
}
