#include "skyline_paths.h"
#include "common/binary_heap.h"
using namespace multi_cost_graph;

namespace representative_paths {
namespace skyline_paths {
vector<Path> NaiveLabelCorrectingSearch::Query(VertexType from, VertexType to, SizeType) {
  const VertexType num_vertices = graph_.NumVertices();
  const SizeType num_dimensions = graph_.NumDimensions();

  vector<vector<unique_ptr<RecursivePath>>> tentative_paths(num_vertices);
  vector<SizeType> done(num_vertices, 0);
  BinaryHeap que(num_vertices);

  tentative_paths[from].emplace_back(new RecursivePath(from, WeightVector(num_dimensions, 0.0), nullptr));
  que.Update(from, 0.0);

  while (!que.IsEmpty()) {
    VertexType v = que.Pop().first;

    const auto &tps = tentative_paths[v];
    for (; done[v] < tps.size(); ++done[v]) {
      const auto &path = tps[done[v]];

      for (SizeType i = 0; i < graph_.Degree(v); ++i) {
        const Edge &edge = graph_.EdgeFrom(v, i);
        if (!edge.HasForward()) continue;

        const WeightVector new_cost = Add(path->Cost(), edge.Cost());

        {
          bool is_dominated = false;
          // Global domination
          for (const auto &current_path : tentative_paths[to]) {
            if ((is_dominated = DoesDominate(current_path->Cost(), new_cost))) break;
          }
          if (is_dominated) continue;
          // Local domination
          for (const auto &current_path : tentative_paths[edge.To()]) {
            if ((is_dominated = DoesDominate(current_path->Cost(), new_cost))) break;
          }
          if (is_dominated) continue;
        }

        tentative_paths[edge.To()].emplace_back(new RecursivePath(edge.To(), new_cost, path.get()));
        que.Update(edge.To(), Dot(new_cost, WeightVector(num_dimensions, 1.0)));
      }
    }
  }

  vector<Path> paths;
  {
    const auto &tps = tentative_paths[to];
    for (SizeType i = 0; i < tps.size(); ++i) {
      const auto &path = tps[i];
      bool is_dominated = false;
      for (SizeType j = 0; j < tps.size(); ++j) {
        if (i == j) continue;
        if ((is_dominated = DoesDominate(tps[j]->Cost(), path->Cost()))) break;
      }
      if (is_dominated) continue;
      paths.emplace_back(path->ConvertToPath());
    }
    sort(all(paths));
  }
  return paths;
}

vector<Path> ParetoPrep::Query(VertexType from, VertexType to, SizeType) {
  //
  // Algorithm 2 in [Shekelyan+, 2014].
  //

  // step1: initialization
  Visit(to);
  lower_bound_[to].assign(num_dimensions_, 0.0);
  que_.Update(to, 0.0);
  for (SizeType d = 0; d < num_dimensions_; ++d) {
    paths_[to][d].reset(new RecursivePath(to, WeightVector(num_dimensions_, 0.0), nullptr));
  }

  while (!que_.IsEmpty()) {
    // step 2: node selection
    VertexType v = que_.Pop().first;

    { // step 3: global domination
      bool is_dominated = false;
      for (const auto &path : paths_[from]) {
        if ((is_dominated = DoesDominate(path->Cost(), lower_bound_[v]))) break;
      }
      if (is_dominated) continue;
    }

    // step 4: node expansion
    for (SizeType i = 0; i < graph_.Degree(v); ++i) {
      const Edge &e = graph_.EdgeFrom(v, i);
      if (!e.HasBackward()) continue;
      VertexType p = e.To();

      for (SizeType d = 0; d < num_dimensions_; ++d) {
        if (lower_bound_[v][d] == kInfinityWeight) continue;
        if (lower_bound_[v][d] + e.Cost()[d] < lower_bound_[p][d]) {
          Visit(p);

          lower_bound_[p][d] = lower_bound_[v][d] + e.Cost()[d];
          que_.Update(p, Dot(lower_bound_[p], WeightVector(num_dimensions_, 1.0)));

          const auto &path = paths_[v][d];
          paths_[p][d].reset(new RecursivePath(p, Add(path->Cost(), e.Cost()), path.get()));
        }
      }
    }

    // (step 5: path construction)
    // --- unnecessary here, |paths_[from]| exactly represents the paths
  }
  JLOG_ADD("skyline_pp.visited_vertices", visited_vertices_.size());

  if (!is_visited_[from]) return vector<Path>();  // Unreachable

  //
  // Algorithm 1 in [Shekelyan+, 2014].
  //

  // step 0: clean up
  for (VertexType v : visited_vertices_) paths_[v].clear();
  que_.Reset();

  // step 1: initialization
  paths_[from].emplace_back(new RecursivePath(from, WeightVector(num_dimensions_, 0.0), nullptr));
  que_.Update(from, 0.0);
  assert(is_visited_[from]);

  for (SizeType iter = 0; !que_.IsEmpty(); ++iter) {
    if (iter > 0 && iter % 1000 == 0) {
      printf("ParetoPrep: iter=%lu, paths=%d\n", iter, (int)paths_[to].size());
    }

    // step 2: node selection
    VertexType v = que_.Pop().first;

    const auto &ps = paths_[v];
    for (; num_done_paths_[v] < ps.size(); ++num_done_paths_[v]) {
      const auto &path = ps[num_done_paths_[v]];

      // step 3: mark globally dominated as processed
      {
        bool is_globally_dominated = false;
        for (const auto &global_path : paths_[to]) {
          WeightVector lb(num_dimensions_);
          for (SizeType d = 0; d < num_dimensions_; ++d) {
            lb[d] = max(lower_bound_[from][d], path->Cost()[d] + lower_bound_[v][d]);
          }
          if ((is_globally_dominated = DoesDominate(global_path->Cost(), lb))) break;
        }
        if (is_globally_dominated) continue;
      }

      // step 4: extend unprocessed paths
      for (SizeType i = 0; i < graph_.Degree(v); ++i) {
        const Edge &edge = graph_.EdgeFrom(v, i);
        if (!edge.HasForward() || !is_visited_[edge.To()]) continue;

        const WeightVector new_cost = Add(path->Cost(), edge.Cost());
        {
          bool is_locally_dominated = false;
          for (const auto &local_path : paths_[edge.To()]) {
            if ((is_locally_dominated = DoesDominate(local_path->Cost(), new_cost))) break;
          }
          if (is_locally_dominated) continue;
        }

        paths_[edge.To()].emplace_back(new RecursivePath(edge.To(), new_cost, path.get()));

        que_.Update(edge.To(), Dot(Add(new_cost, lower_bound_[edge.To()]), WeightVector(num_dimensions_, 1.0)));
      }
    }
  }

  vector<Path> paths;
  {
    const auto &tps = paths_[to];
    for (SizeType i = 0; i < tps.size(); ++i) {
      const auto &path = tps[i];
      bool is_dominated = false;
      for (SizeType j = 0; j < tps.size(); ++j) {
        if (i == j) continue;
        if ((is_dominated = DoesDominate(tps[j]->Cost(), path->Cost()))) break;
      }
      if (is_dominated) continue;
      paths.emplace_back(path->ConvertToPath());
    }
    sort(all(paths));
  }

  Reset();
  return paths;
}
}  // namespace skyline
}  // namespace representative_paths
